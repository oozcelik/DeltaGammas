import os

from Configurables import (DaVinci, CombineParticles, GaudiSequencer)

from Configurables import (
    DecayTreeTuple,
    LoKi__Hybrid__TupleTool,
    LoKi__Hybrid__EvtTupleTool,
    TupleToolDecay,
    TupleToolEventInfo,
    TupleToolKinematic,
    TupleToolGeometry,
    TupleToolPid,
    TupleToolAngles,
    TupleToolGamma,
    TupleToolVeto,
    TupleToolVtxIsoln,
    TupleToolConeIsolation,
    #TupleToolTrackIsolation,
    TupleToolTISTOS,
    TupleToolDecayTreeFitter,
    TupleToolProtoPData,
    TupleToolPhotonInfo,
    TupleToolPi0Info,
    TupleToolMCTruth,
    MCTupleToolHierarchy,
    MCDecayTreeTuple,
    TupleToolMCBackgroundInfo)

from DecayTreeTuple.Configuration import *

from MVADictHelpers import *

from PhysSelPython.Wrappers import AutomaticData, Selection, FilterSelection, SelectionSequence, TupleSelection

#--------------------
#--------------------
#tools config--------
#--------------------
#--------------------

dtf_origin = True
dtf_update = True
dtf_verbose = False

saveisol = False
trigger_info = True

neutralveto = True

saveallpid = True

#--------------------
#--------------------

#standard tupletools---------

#stdtool = ["TupleToolAngles","TupleToolEventInfo","TupleToolKinematic","TupleToolGeometry"]

pidvar = {
    "ID": "ID",
    "PROBNNk": "PROBNNk",
    "PROBNNpi": "PROBNNpi",
    "PROBNNp": "PROBNNp",
    "PROBNNe": "PROBNNe",
    "PROBNNmu": "PROBNNmu",
    "PROBNNghost": "PROBNNghost",
    "PIDK": "PIDK",
    "PIDp": "PIDp",
    "PIDe": "PIDe",
    "PIDmu": "PIDmu"
}

MCT = TupleToolMCTruth("MCT")
MCT.ToolList = [
    "MCTupleToolKinematic", "MCTupleToolHierarchy", "MCTupleToolReconstructed"
]

#neutral variabes------------

Neutrals = TupleToolProtoPData("Neutrals")
Neutrals.DataList = [
    "CaloNeutralID", "CaloNeutralSpd", "IsNotH", "IsNotE", "IsPhoton"
]

GammaVeto = TupleToolGamma("GammaVeto")
GammaVeto.Veto["Pi0"] = ["Phys/StdLoosePi02gg/Particles"]
GammaVeto.Veto["Eta"] = ["Phys/StdLooseEta2gg/Particles"]
GammaVeto.Combi = 2

VetoG = TupleToolVeto("VetoG")
VetoG.Particle = "gamma"
VetoG.Veto["Pi02gg"] = ["/Event/Phys/StdLoosePi02gg/Particles"]
VetoG.Veto["Eta2gg"] = ["/Event/Phys/StdLooseEta2gg/Particles"]
VetoG.Veto["Pi0R"] = ["/Event/Phys/StdLooseResolvedPi0/Particles"]
VetoG.Veto["EtaR"] = ["/Event/Phys/StdLooseResolvedEta/Particles"]
VetoG.Veto["Pi0M"] = ["/Event/Phys/StdLooseMergedPi0/Particles"]

VetoR = TupleToolVeto("VetoR")
VetoR.Particle = "pi0"
VetoR.VetoOther["Pi0R"] = ["/Event/Phys/StdLooseResolvedPi0/Particles"]
VetoR.VetoOther["Pi02gg"] = ["/Event/Phys/StdLoosePi02gg/Particles"]

VetoM = TupleToolVeto("VetoM")
VetoM.Particle = "pi0"
VetoM.Veto["Gamma"] = ["/Event/Phys/StdLooseAllPhotons/Particles"]
VetoM.Veto["Pi0R"] = ["/Event/Phys/StdLooseResolvedPi0/Particles"]
VetoM.Veto["EtaR"] = ["/Event/Phys/StdLooseResolvedEta/Particles"]
VetoM.Veto["Pi02gg"] = ["/Event/Phys/StdLoosePi02gg/Particles"]
VetoM.Veto["Eta2gg"] = ["/Event/Phys/StdLooseEta2gg/Particles"]

VetoE = TupleToolVeto("VetoE")
VetoE.Particle = "eta"
VetoE.VetoOther["EtaR"] = ["/Event/Phys/StdLooseResolvedEta/Particles"]
VetoE.VetoOther["Eta2gg"] = ["/Event/Phys/StdLooseEta2gg/Particles"]

#event variables-------

RecoStat = LoKi__Hybrid__EvtTupleTool("RecoStat")

recostatvar = {
    "nSPDHits": "CONTAINS( 'Raw/Spd/Digits')",
    "nPhotons": "CONTAINS( 'Phys/StdLooseAllPhotons/Particles' )",
    "nTracks": "CONTAINS( 'Rec/Track/Best') ",
    "nPVs": "CONTAINS( 'Rec/Vertex/Primary')",
    "nPhotons": "CONTAINS( 'Phys/StdLooseAllPhotons/Particles' )",
    "nPi0R": "CONTAINS( 'Phys/StdLooseResolvedPi0/Particles' )",
    "nPi0M": "CONTAINS( 'Phys/StdLooseMergedPi0/Particles' )",
    "LoKi_nVeloClusters": "CONTAINS('Raw/Velo/Clusters')",
    "LoKi_nITClusters": "CONTAINS('Raw/IT/Clusters')",
    "LoKi_nTTClusters": "CONTAINS('Raw/TT/Clusters')",
    "LoKi_nOThits": "CONTAINS('Raw/OT/Times')"
}

RecoStat.VOID_Variables = recostatvar

#isolation variables--------

VtxIsol = TupleToolVtxIsoln("VtxIsol")

#ConeIsol = TupleToolConeIsolation('ConeIsol',
#                                  FillComponents=True,
#                                  MinConeSize=0.4,
#                                  SizeStep=0.3,
#                                  MaxConeSize=1.6,
#                                  Verbose=True,
#                                  FillPi0Info=False,
#                                  FillMergedPi0Info=False,
#                                  #nominally muons from SL analyses, but we are missing pions...
#                                  ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
#                                  MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles"
#                                  )

ConeIsol = TupleToolConeIsolation(
    'ConeIsol',
    FillComponents=True,
    MinConeSize=1.8,
    SizeStep=0.1,
    MaxConeSize=1.9,
    Verbose=True,
    FillAsymmetry=True,
    FillPi0Info=False,
    FillMergedPi0Info=False,
    #nominally muons from SL analyses, but we are missing pions...
    ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles",
    MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles")

#TrkIsol = TupleToolTrackIsolation('TrkIsol',
#                                  MinConeAngle=0.4,
#                                  StepSize=0.3,
#                                  MaxConeAngle=1.6,
#                                  Verbose=True
#                                  )

#trigger---------

L0_lines = ['L0MuonDecision', 'L0DiMuonDecision', 'L0GlobalDecision']

L1_lines = [
    'Hlt1DiMuonHighMassDecision', 'Hlt1DiMuonLowMassDecision',
    'Hlt1DiMuonNoL0Decision', 'Hlt1GlobalDecision', 'Hlt1L0AnyDecision',
    'Hlt1SingleMuonHighPTDecision', 'Hlt1TrackMVADecision',
    'Hlt1TrackMVALooseDecision', 'Hlt1TrackMuonDecision',
    'Hlt1TrackMuonMVADecision', 'Hlt1TwoTrackMVADecision',
    'Hlt1TwoTrackMVALooseDecision', 'Hlt1SingleMuonNoIPDecision',
    'Hlt1TrackAllL0Decision'
]

L2_lines = [
    'Hlt2DiMuonDecision',
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedHeavyDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2DiMuonDetachedPsi2SDecision',
    'Hlt2DiMuonJPsiDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2SingleMuonHighPTDecision',
    'Hlt2SingleMuonLowPTDecision',
    'Hlt2SingleMuonRareDecision',
    'Hlt2SingleMuonVHighPTDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2TopoMuMu2BodyDecision',
    'Hlt2TopoMuMu3BodyDecision',
    'Hlt2TopoMuMu4BodyDecision',
    'Hlt2Topo2BodySimpleDecision',
    'Hlt2Topo3BodySimpleDecision',
    'Hlt2Topo4BodySimpleDecision',
    'Hlt2Topo2BodyBBDTDecision',
    'Hlt2Topo3BodyBBDTDecision',
    'Hlt2Topo4BodyBBDTDecision',
    'Hlt2TopoMu2BodyBBDTDecision',
    'Hlt2TopoMu3BodyBBDTDecision',
    'Hlt2TopoMu4BodyBBDTDecision',
    'Hlt2TopoE2BodyBBDTDecision',
    'Hlt2TopoE3BodyBBDTDecision',
    'Hlt2TopoE4BodyBBDTDecision',
]

triglist = L0_lines + L1_lines + L2_lines
#triglist = L0_lines

TISTOS = TupleToolTISTOS("TISTOS")
TISTOS.VerboseL0 = True
TISTOS.VerboseHlt1 = True
TISTOS.VerboseHlt2 = True
TISTOS.TriggerList = triglist[:]

#-----------------------
#LoKi stuff
LoKiVariables = LoKi__Hybrid__TupleTool('LoKiVariables')
LoKiVariables.Variables = {"BPVLTIME": "BPVLTIME()"}

LoKiVariablesJpsi = LoKi__Hybrid__TupleTool('LoKiVariablesJpsi')
LoKiVariablesJpsi.Variables = {"DLS": "BPVDLS"}

#-----------------------


def tuple_maker(name, decay, branches, input_obj, dtf_massconst, neutral,
                domva):
    """Return a sequence for producing ntuples."""

    dtt = DecayTreeTuple(name + '_Tuple')

    dtt.Decay = decay
    dtt.Inputs = [input_obj]

    dtt.addBranches(branches)

    dtt.ToolList = [
        "TupleToolAngles", "TupleToolEventInfo", "TupleToolKinematic",
        "TupleToolTrackInfo"
    ]

    geom = dtt.addTupleTool('TupleToolGeometry')
    geom.RefitPVs = True
    geom.Verbose = True

    if saveallpid:
        dtt.addTupleTool("TupleToolPid")
    else:
        if "mu1" in branches:
            LoKi_mu1 = dtt.mu1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_mu1")
            LoKi_mu1.Variables = pidvar
        if "mu2" in branches:
            LoKi_mu2 = dtt.mu2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_mu2")
            LoKi_mu2.Variables = pidvar
        if "pi1" in branches:
            LoKi_pi1 = dtt.pi1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_pi1")
            LoKi_pi1.Variables = pidvar
        if "pi2" in branches:
            LoKi_pi2 = dtt.pi2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_pi2")
            LoKi_pi2.Variables = pidvar
        if "K" in branches:
            LoKi_K = dtt.K.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_K")
            LoKi_K.Variables = pidvar
        if "pi" in branches:
            LoKi_pi = dtt.pi.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_pi")
            LoKi_pi.Variables = pidvar

    if "gamma" in branches:
        dtt.gamma.addTupleTool(Neutrals)
        dtt.gamma.addTupleTool("TupleToolPhotonInfo")
        if neutralveto:
            dtt.gamma.addTupleTool(GammaVeto)
            dtt.gamma.addTupleTool(VetoG)

    if "gamma0" in branches:
        dtt.gamma0.addTupleTool(Neutrals)
        dtt.gamma0.addTupleTool("TupleToolPhotonInfo")
        if neutralveto:
            dtt.gamma0.addTupleTool(GammaVeto)
            dtt.gamma0.addTupleTool(VetoG)

    if "pi0" in branches:
        dtt.pi0.addTupleTool(Neutrals)
        dtt.pi0.addTupleTool("TupleToolPi0Info")
        if neutralveto:
            dtt.pi0.addTupleTool(VetoR)
            dtt.pi0.addTupleTool(VetoM)

    if "eta" in branches:
        if neutralveto:
            dtt.eta.addTupleTool(VetoE)

    if "Jpsi" in branches:
        dtt.Jpsi.addTool(LoKiVariablesJpsi)
        dtt.Jpsi.ToolList += ['LoKi::Hybrid::TupleTool/LoKiVariablesJpsi']

    dtt.addTupleTool(RecoStat)

    if DaVinci().Simulation:
        dtt.addTupleTool(MCT)
        dtt.ToolList += ['TupleToolMCBackgroundInfo']

    if "Bs" in branches:
        if trigger_info:
            dtt.Bs.addTupleTool(TISTOS)
            dtt.Jpsi.addTupleTool(TISTOS)

        if domva:
            addTMVAclassifierTuple(dtt.Bs,
                                   domva[0],
                                   domva[1],
                                   "Fisher",
                                   False,
                                   Preambulo=[""])

        dtt.Bs.addTupleTool(VtxIsol)
        dtt.Bs.addTupleTool(ConeIsol)

        dtt.Bs.addTupleTool('TupleToolDecayTreeFitter/DTF')
        dtt.Bs.DTF.constrainToOriginVertex = dtf_origin
        dtt.Bs.DTF.Verbose = dtf_verbose
        dtt.Bs.DTF.UpdateDaughters = dtf_update
        dtt.Bs.DTF.daughtersToConstrain = dtf_massconst

        dtt.Bs.addTupleTool('TupleToolDecayTreeFitter/DTFNoCon')
        dtt.Bs.DTFNoCon.constrainToOriginVertex = dtf_origin
        dtt.Bs.DTFNoCon.Verbose = dtf_verbose
        dtt.Bs.DTFNoCon.UpdateDaughters = dtf_update

        dtt.Bs.addTool(LoKiVariables)
        dtt.Bs.ToolList += ['LoKi::Hybrid::TupleTool/LoKiVariables']

    else:
        if trigger_info:
            dtt.B.addTupleTool(TISTOS)
            dtt.Jpsi.addTupleTool(TISTOS)

        if domva:
            addTMVAclassifierTuple(dtt.B,
                                   domva[0],
                                   domva[1],
                                   "Fisher",
                                   False,
                                   Preambulo=[""])

        dtt.B.addTupleTool(VtxIsol)
        dtt.B.addTupleTool(ConeIsol)

        dtt.B.addTupleTool('TupleToolDecayTreeFitter/DTF')
        dtt.B.DTF.constrainToOriginVertex = dtf_origin
        dtt.B.DTF.Verbose = dtf_verbose
        dtt.B.DTF.UpdateDaughters = dtf_update
        dtt.B.DTF.daughtersToConstrain = dtf_massconst

    return dtt


def combi_maker(combi_name, decay, daugcut, combicut, mothcut, inputpart,
                domva, dorefit):
    """Return a sequence for combining particles."""

    combi = CombineParticles(combi_name,
                             DecayDescriptor=decay,
                             DaughtersCuts=daugcut,
                             CombinationCut=combicut,
                             MotherCut=mothcut,
                             ReFitPVs=dorefit,
                             Preambulo=[
                                 "from LoKiPhysMC.decorators import *",
                                 "from LoKiPhysMC.functions import mcMatch"
                             ])

    if domva: addTMVAclassifierValue(combi, domva[0], domva[1], "Fisher")

    combi_sel = Selection(combi_name + "_Sel",
                          Algorithm=combi,
                          RequiredSelections=inputpart)

    return combi_sel