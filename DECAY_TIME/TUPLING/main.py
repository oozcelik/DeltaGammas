import os
import sys
sys.path.append(os.path.join('.'))
sys.path.append(os.path.join('./combiners'))

from Configurables import DaVinci, GaudiSequencer, DecayTreeTuple
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import FilterDesktop, CombineParticles
from MVADictHelpers import *

# From Matt
from Configurables import MCDecayTreeTuple

import prodtools

#decays to tuple----------
from decaytotuple import *

#tmva fisher cut---------

applyfisher = False
savefisher = False
jpsi_sideband = False
jpsi_no_dls = True
dorefit = True
applystrip = False

#-----------------------------
#-------STRIPPING-------------
#-----------------------------
#-----------------------------
#for MC LDST only (JpsiK)
#-----------------------------

if DaVinci().DataType == '2011': stripping = 'stripping21r1'

if DaVinci().DataType == '2012': stripping = 'stripping21'

if DaVinci().DataType == '2015': stripping = 'stripping24r2'

if DaVinci().DataType == '2016': stripping = 'stripping28r2'

if DaVinci().DataType == '2017': stripping = 'stripping29r2'

if DaVinci().DataType == '2018': stripping = 'stripping34'

if applystrip:

    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive

    config = strippingConfiguration(stripping)
    archive = strippingArchive(stripping)
    streams = buildStreams(stripping=config, archive=archive)

    # Select my line
    MyStream = StrippingStream("Dimuon")
    MyLines = ['StrippingFullDSTDiMuonJpsi2MuMuDetachedLine']

    for stream in streams:
        for line in stream.lines:
            if line.name() in MyLines:
                MyStream.appendLines([line])

                # Configure Stripping
                from Configurables import ProcStatusCheck
                filterBadEvents = ProcStatusCheck()

                stripconf = StrippingConf(Streams=[MyStream],
                                          MaxCandidates=2000,
                                          AcceptBadEvents=False,
                                          BadEventSelection=filterBadEvents)

#-----------------------------
#-------CONTAINERS------------
#-----------------------------
#-----------------------------
#get particle locations
#don't apply PID cuts for MC to later apply calibration
#-----------------------------

if DaVinci().Simulation:
    prefixTES = '/Event/AllStreams/'
    if applystrip: prefixTES = ''
else:
    prefixTES = '/Event/Dimuon/'

StripJpsi = AutomaticData(prefixTES +
                          'Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles')
StripJpsiNoDLS = AutomaticData('Phys/StdLooseJpsi2MuMu/Particles')
StripKaons = AutomaticData('Phys/StdLooseKaons/Particles')
#StripPions    = AutomaticData('Phys/StdLoosePions/Particles')
StripPions = AutomaticData(
    'Phys/StdAllNoPIDsPions/Particles')  # Pions with no IP cut
StripPhotons = AutomaticData('Phys/StdLooseAllPhotons/Particles')
StripPi02gg = AutomaticData('Phys/StdLoosePi02gg/Particles')
StripPi0M = AutomaticData('Phys/StdLooseMergedPi0/Particles')
StripEta2gg = AutomaticData('Phys/StdLooseEta2gg/Particles')

if jpsi_sideband:
    cut_Jpsi = FilterDesktop('cut_Jpsi_Filter', Code="(M>3150*MeV)")
    Jpsi = Selection('Jpsi_Sel',
                     Algorithm=cut_Jpsi,
                     RequiredSelections=[StripJpsi])
if jpsi_no_dls:
    cut_Jpsi = FilterDesktop('cut_Jpsi_Filter',
                             Code="""(CHILD(PT,1)>500*MeV) & \
                                    (CHILD(PT,2)>500*MeV) & \
                                    (MINTREE('mu+'==ABSID,PIDmu) > 0.0) & \
                                    ((MM > 3046.9) & (MM < 3146.9)) & \
                                    (VFASPF(VCHI2PDOF)< 20.0) & \
                                    (MAXTREE('mu+'==ABSID, TRGHP)<0.2)""")
    Jpsi = Selection('Jpsi_Sel',
                     Algorithm=cut_Jpsi,
                     RequiredSelections=[StripJpsiNoDLS])
else:
    Jpsi = StripJpsi

#apply PID for data only--------
if DaVinci().Simulation:
    Kaons = StripKaons
    Pions = StripPions
    Photons = StripPhotons
    Pi02gg = StripPi02gg
    Pi0M = StripPi0M
    Eta2gg = StripEta2gg
else:
    pid_Kaons = FilterDesktop('pid_Kaons_Filter', Code='(PROBNNk>0.1)')
    Kaons = Selection('Kaons_Sel',
                      Algorithm=pid_Kaons,
                      RequiredSelections=[StripKaons])

    pid_Pions = FilterDesktop('pid_Pions_Filter',
                              Code='(PROBNNpi>0.2) & (TRGHP < 0.2)')
    Pions = Selection('Pions_Sel',
                      Algorithm=pid_Pions,
                      RequiredSelections=[StripPions])

    pid_g = FilterDesktop('pid_g_Filter', Code='(CL>0.05)')
    Photons = Selection('Photons_Sel',
                        Algorithm=pid_g,
                        RequiredSelections=[StripPhotons])

    pid_gg = FilterDesktop('pid_gg_Filter',
                           Code='(CHILD(CL,1)>0.05) & (CHILD(CL,2)>0.05)')
    Pi02gg = Selection('Pi02gg_Sel',
                       Algorithm=pid_gg,
                       RequiredSelections=[StripPi02gg])
    Pi0M = StripPi0M
    Eta2gg = Selection('Eta2gg_Sel',
                       Algorithm=pid_gg,
                       RequiredSelections=[StripEta2gg])
    Jpsi = StripJpsi

#-----------------------------
#-----------------------------
#--------SELECTIONS-----------
#-----------------------------
#-----------------------------
#Usual cuts to be used in combine particles
#-----------------------------

jpsik_dtf = ['J/psi(1S)']
jpsikst0_dtf = ['J/psi(1S)']
jpsirho0_dtf = ['J/psi(1S)']

chic1k_dtf = ['J/psi(1S)', 'chi_c1(1P)']

jpsipi0_dtf = ['J/psi(1S)', 'pi0']
jpsikst_dtf = ['J/psi(1S)', 'pi0']

jpsietar_dtf = ['J/psi(1S)', 'eta']
jpsieta2pipipi0_dtf = ['J/psi(1S)', 'eta', 'pi0']
jpsieta2pipig_dtf = ['J/psi(1S)', 'eta']

jpsietap2rho0g_dtf = ['J/psi(1S)', 'eta_prime']
jpsietap2pipieta_dtf = ['J/psi(1S)', 'eta_prime', 'eta']

jpsif02pipi_dtf = ['J/psi(1S)']  # To constrain DTF

#-----rho0[pipi]-------

from Rho02PiPi import rho02pipi_name, rho02pipi_decd, rho02pipi_daug, rho02pipi_comb, rho02pipi_moth

rho02pipi_input = [Pions]
rho02pipi_sel = prodtools.combi_maker(rho02pipi_name, rho02pipi_decd,
                                      rho02pipi_daug, rho02pipi_comb,
                                      rho02pipi_moth, rho02pipi_input, False,
                                      False)

#-----etap[rho0g]-------

from Etap2Rho0Gamma import etap2rho0g_name, etap2rho0g_decd, etap2rho0g_daug, etap2rho0g_comb, etap2rho0g_moth

etap2rho0g_input = [rho02pipi_sel, Photons]
etap2rho0g_sel = prodtools.combi_maker(etap2rho0g_name, etap2rho0g_decd,
                                       etap2rho0g_daug, etap2rho0g_comb,
                                       etap2rho0g_moth, etap2rho0g_input,
                                       False, False)

#-----jpsietap[rho0g]-------

from JpsiEtap2Rho0G import jpsietap2rho0g_name, jpsietap2rho0g_decd, jpsietap2rho0g_decd2, jpsietap2rho0g_branches, jpsietap2rho0g_daug, jpsietap2rho0g_comb, jpsietap2rho0g_moth

jpsietap2rho0g_input = [Jpsi, etap2rho0g_sel]
jpsietap2rho0g_sel = prodtools.combi_maker(
    jpsietap2rho0g_name, jpsietap2rho0g_decd, jpsietap2rho0g_daug,
    jpsietap2rho0g_comb, jpsietap2rho0g_moth, jpsietap2rho0g_input, False,
    dorefit)
jpsietap2rho0g_seq = SelectionSequence(jpsietap2rho0g_name + "_Seq",
                                       TopSelection=jpsietap2rho0g_sel)
jpsietap2rho0g_tuple = prodtools.tuple_maker(
    jpsietap2rho0g_name, jpsietap2rho0g_decd2, jpsietap2rho0g_branches,
    jpsietap2rho0g_seq.outputLocation(), jpsietap2rho0g_dtf, "", False)
#jpsietap2rho0g_tuple_check_refitted_pv = FilterDesktop('CheckReFitPV',
#                                                        Code='BPVVALID()',
#                                                        Inputs=jpsietap2rho0g_tuple.Inputs,
#                                                        ReFitPVs=True)

if DaVinci().Simulation and TupleJpsiEtap2Rho0G and not TupleBkgJpsiEtap2PiPi:
    jpsietap2rho0g_mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
    jpsietap2rho0g_mctuple.Decay = jpsietap2rho0g_decd2
    jpsietap2rho0g_mctuple.ToolList = [
        "MCTupleToolKinematic", "MCTupleToolReconstructed"
    ]
    DaVinci().UserAlgorithms += [jpsietap2rho0g_mctuple]

#-----f0[pipi]-------

from F02PiPi import *

f02pipi_input = [Pions]
f02pipi_sel = prodtools.combi_maker(f02pipi_name, f02pipi_decd, f02pipi_daug,
                                    f02pipi_comb, f02pipi_moth, f02pipi_input,
                                    False, False)

#-----jpsif0[pipi]-------

from JpsiF02PiPi import *

jpsif02pipi_input = [Jpsi, f02pipi_sel]
jpsif02pipi_sel = prodtools.combi_maker(jpsif02pipi_name, jpsif02pipi_decd,
                                        jpsif02pipi_daug, jpsif02pipi_comb,
                                        jpsif02pipi_moth, jpsif02pipi_input,
                                        False, dorefit)
jpsif02pipi_seq = SelectionSequence(jpsif02pipi_name + "_Seq",
                                    TopSelection=jpsif02pipi_sel)
jpsif02pipi_tuple = prodtools.tuple_maker(jpsif02pipi_name, jpsif02pipi_decd2,
                                          jpsif02pipi_branches,
                                          jpsif02pipi_seq.outputLocation(),
                                          jpsif02pipi_dtf, "", False)
#jpsif02pipi_tuple_check_refitted_pv = FilterDesktop('CheckReFitPV',
#                                                     Code = 'BPVVALID()',
#                                                     Inputs = jpsif02pipi_tuple.Inputs,
#                                                     ReFitPVs = True)

if DaVinci().Simulation and TupleJpsiF02PiPi and not TupleBkgJpsiF02PiPi:
    jpsif02pipi_mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
    jpsif02pipi_mctuple.Decay = jpsif02pipi_mcdecay
    jpsif02pipi_mctuple.ToolList = [
        "MCTupleToolKinematic", "MCTupleToolReconstructed"
    ]
    DaVinci().UserAlgorithms += [jpsif02pipi_mctuple]

#sequences

from Configurables import TrackScaleState
scaler = TrackScaleState()
if DaVinci().Simulation == False:
    DaVinci().UserAlgorithms += [scaler]

if applystrip:
    DaVinci().appendToMainSequence([stripconf.sequence()])

Seq_JpsiEtap2Rho0G = GaudiSequencer("Phys/Seq_JpsiEtap2Rho0G")
#Seq_JpsiEtap2Rho0G.Members += [jpsietap2rho0g_seq.sequence(), jpsietap2rho0g_tuple_check_refitted_pv, jpsietap2rho0g_tuple]
Seq_JpsiEtap2Rho0G.Members += [
    jpsietap2rho0g_seq.sequence(), jpsietap2rho0g_tuple
]

Seq_JpsiF02PiPi = GaudiSequencer("Phys/Seq_JpsiF02PiPi")
Seq_JpsiF02PiPi.Members += [jpsif02pipi_seq.sequence(), jpsif02pipi_tuple]
#Seq_JpsiF02PiPi.Members += [jpsif02pipi_seq.sequence(), jpsif02pipi_tuple_check_refitted_pv, jpsif02pipi_tuple]

if TupleJpsiEtap2Rho0G:
    DaVinci().UserAlgorithms += [Seq_JpsiEtap2Rho0G]

if TupleJpsiF02PiPi:
    DaVinci().UserAlgorithms += [Seq_JpsiF02PiPi]