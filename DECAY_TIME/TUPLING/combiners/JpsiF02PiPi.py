jpsif02pipi_name = "B2JpsiF02PiPi"
jpsif02pipi_decd = "B_s0 -> J/psi(1S) f_0(980)"
jpsif02pipi_decd2 = "B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(f_0(980) -> ^pi+ ^pi-)"
jpsif02pipi_mcdecay = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+ ^pi-]CC"
jpsif02pipi_branches = {
    "Bs": "B_s0",
    "Jpsi": "B_s0 -> ^(J/psi(1S) ->  mu+  mu-)   (f_0(980) ->  pi+  pi-)",
    "mu1": "B_s0 ->  (J/psi(1S) -> ^mu+  mu-)   (f_0(980) ->  pi+  pi-)",
    "mu2": "B_s0 ->  (J/psi(1S) ->  mu+ ^mu-)   (f_0(980) ->  pi+  pi-)",
    "f0": "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  ^(f_0(980) ->  pi+  pi-)",
    "pi1": "B_s0 ->  (J/psi(1S) ->  mu+  mu-)   (f_0(980) -> ^pi+  pi-)",
    "pi2": "B_s0 ->  (J/psi(1S) ->  mu+  mu-)   (f_0(980) ->  pi+ ^pi-)"
}
#jpsif02pipi_daug = {"f_0(980)" : "(PT > 0.*GeV) & (mcMatch('Beauty => J/psi(1S) ^f_0(980)'))"}
jpsif02pipi_daug = {}
#jpsif02pipi_comb = "(AM > 4900*MeV) & (AM < 6600*MeV)"
jpsif02pipi_comb = "(AM > 4900*MeV) & (AM < 5700*MeV)"
#jpsif02pipi_moth = "(BPVDIRA > 0.9995) & (BPVIP() < 0.2) & (BPVIPCHI2() < 20) & (BPVLTIME () > 0.25 * picosecond) & (MM > 5000*MeV) & (MM < 6500*MeV)"
#jpsif02pipi_moth = " (BPVLTIME () > 0.25 * picosecond) & (BPVIPCHI2() < 50) & (VFASPF(VCHI2PDOF) < 10) & (MM > 5000*MeV) & (MM < 6500*MeV)"
jpsif02pipi_moth = "(BPVLTIME () > 0.35 * picosecond) & (BPVIPCHI2() < 50) & (VFASPF(VCHI2PDOF) < 8) & (MM > 5000*MeV) & (MM < 6500*MeV) & (DTF_FUN(M, False, strings(['J/psi(1S)','f_0(980)'])) > 5150) & (DTF_FUN(M, False, strings(['J/psi(1S)','f_0(980)'])) < 5650) & (DTF_CHI2NDOF(False, strings(['J/psi(1S)', 'f_0(980)'])) > 0) & (DTF_CHI2NDOF(False, strings(['J/psi(1S)', 'f_0(980)'])) < 5)"

## Background decay descriptors for mcdecay
#bak_jpsif02pipi_bd_jpsi_pipi_decd = "[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^pi+ ^pi-]CC"  # to change bak -> bkg
#bkg_jpsif02pipi_bs_jpsi_kk_decd = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+ ^K-]CC"
#bkg_jpsif02pipi_bs_jpsi_3p_decd = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^pi+ ^pi- ^pi0)]CC"  # to change to jpsi phi 3pi
##bkg_jpsif02pipi_bd_jpsi_kpi_decd = "[[B0]nos -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+ ^pi-]CC, [[B0]os -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K- ^pi+]CC"
##bkg_jpsif02pipi_bd_jpsi_kpi_decd = "[[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+ ^pi-]CC, [B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K- ^pi+]CC"
#bkg_jpsif02pipi_bs_jpsi_phi_kk_decd = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC"