f02pipi_name = "F02PiPi"
f02pipi_decd = "f_0(980) -> pi+ pi-"
f02pipi_daug = {}
f02pipi_comb = "(AM > 900*MeV) & (AM < 1080*MeV) & (ADOCACHI2CUT(30, ''))"
f02pipi_moth = ("(VFASPF(VCHI2) < 20)")