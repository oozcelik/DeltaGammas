rho02pipi_name = "Rho02PiPi"
rho02pipi_decd = "rho(770)0 -> pi+ pi-"
rho02pipi_daug = {}
rho02pipi_comb = "(AM > 625*MeV) & (AM < 925*MeV) & (ADOCACHI2CUT(30, ''))"
rho02pipi_moth = ("(VFASPF(VCHI2) < 20)")