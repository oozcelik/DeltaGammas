jpsietap2rho0g_name = "B2JpsiEtap2Rho0G"
jpsietap2rho0g_decd = "B_s0 -> J/psi(1S) eta_prime"
jpsietap2rho0g_decd2 = "B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(eta_prime -> ^(rho(770)0 -> ^pi+ ^pi-) ^gamma)"
jpsietap2rho0g_branches = {
    "Bs"     : "B_s0",
    "Jpsi"   : "B_s0 -> ^(J/psi(1S) ->  mu+  mu-)  (eta_prime ->  (rho(770)0 ->  pi+  pi-)  gamma)",
    "mu1"    : "B_s0 ->  (J/psi(1S) -> ^mu+  mu-)  (eta_prime ->  (rho(770)0 ->  pi+  pi-)  gamma)", 
    "mu2"    : "B_s0 ->  (J/psi(1S) ->  mu+ ^mu-)  (eta_prime ->  (rho(770)0 ->  pi+  pi-)  gamma)",
    "etap"   : "B_s0 ->  (J/psi(1S) ->  mu+  mu-) ^(eta_prime ->  (rho(770)0 ->  pi+  pi-)  gamma)",
    "rho0"   : "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (eta_prime -> ^(rho(770)0 ->  pi+  pi-)  gamma)",
    "pi1"    : "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (eta_prime ->  (rho(770)0 -> ^pi+  pi-)  gamma)",
    "pi2"    : "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (eta_prime ->  (rho(770)0 ->  pi+ ^pi-)  gamma)",
    "gamma"  : "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (eta_prime ->  (rho(770)0 ->  pi+  pi-) ^gamma)"}
#jpsietap2rho0g_daug = {"eta_prime" : "(PT>0.*GeV) & (mcMatch('Beauty => J/psi(1S) ^eta_prime'))"}
jpsietap2rho0g_daug = {}
jpsietap2rho0g_comb = "(AM > 4900*MeV) & (AM < 5700*MeV)"
jpsietap2rho0g_moth = "(BPVLTIME () > 0.35 * picosecond) & (BPVIPCHI2() < 50) & (VFASPF(VCHI2PDOF) < 8) & (MM > 5000*MeV) & (MM < 6500*MeV) & (DTF_FUN(M, False, strings(['J/psi(1S)','eta_prime'])) > 5150) & (DTF_FUN(M, False, strings(['J/psi(1S)','eta_prime'])) < 5650) & (DTF_CHI2NDOF(False, strings(['J/psi(1S)', 'eta_prime'])) > 0) & (DTF_CHI2NDOF(False, strings(['J/psi(1S)', 'eta_prime'])) < 5)"
# & ( DTF_CTAUERR( 0, True )/0.299792458 > 0.5)"