etap2rho0g_name = "Etap2Rho0G"
etap2rho0g_decd = "eta_prime -> rho(770)0 gamma"
etap2rho0g_daug = {"gamma": "(CL > 0.2) & (PT > 500*MeV)"}
etap2rho0g_comb = "(ADAMASS('eta_prime') < 100*MeV) & (APT > 2000*MeV)"
etap2rho0g_moth = ("ALL")