jpsirho0_name = "B2JpsiRho0"
jpsirho0_decd = "B_s0 -> J/psi(1S) rho(770)0"
jpsirho0_decd2 = "B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(rho(770)0 -> ^pi+ ^pi-)"
jpsirho0_branches = {
    "B"      : "B_s0",
    "Jpsi"   : "B_s0 -> ^(J/psi(1S) ->  mu+  mu-)  (rho(770)0 ->  pi+  pi-)",
    "mu1"    : "B_s0 ->  (J/psi(1S) -> ^mu+  mu-)  (rho(770)0 ->  pi+  pi-)",
    "mu2"    : "B_s0 ->  (J/psi(1S) ->  mu+ ^mu-)  (rho(770)0 ->  pi+  pi-)",
    "rho0"   : "B_s0 ->  (J/psi(1S) ->  mu+  mu-) ^(rho(770)0 ->  pi+  pi-)",
    "pi1"    : "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (rho(770)0 -> ^pi+  pi-)",
    "pi2"    : "B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (rho(770)0 ->  pi+ ^pi-)"}
jpsirho0_daug = {}
jpsirho0_comb = "(AM>4900*MeV) & (AM<6100*MeV)"
jpsirho0_moth = "(BPVDIRA>0.9995) & (BPVIP()<0.2) & (BPVIPCHI2()<20) & (VFASPF(VCHI2PDOF)<10) & (MM>5000*MeV) & (MM<6000*MeV)"
