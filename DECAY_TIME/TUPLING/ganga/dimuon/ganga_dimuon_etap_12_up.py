splitter = SplitByFiles()
#splitter.maxFiles = 100000
splitter.filesPerJob = 1
splitter.bulksubmit = False
splitter.ignoremissing = True

j = Job(name="EP12-UP-MC", splitter=splitter, comment="v1.9.2")

myApp = GaudiExec()
#myApp.directory = "./DaVinciDev_v45r2"
myApp.directory = "/afs/cern.ch/work/p/petruccs/lhcb/analysis/DaVinci/DaVinciDev_v45r2"

j.application = myApp
j.application.platform = "x86_64-centos7-gcc8-opt"
j.application.options = ["options_dimuon_etap_12_up.py", "../../main.py"]

j.inputfiles = [
    "../../decaytotuple.py",
    "../../prodtools.py",
    # f0 stuff
    "../../combiners/F02PiPi.py",
    "../../combiners/JpsiF02PiPi.py",
    # Etap stuff
    "../../combiners/Rho02PiPi.py",
    "../../combiners/Etap2Rho0Gamma.py",
    "../../combiners/JpsiEtap2Rho0G.py"
]

path = "/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09j/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/13144201/ALLSTREAMS.DST"

j.inputdata = BKQuery(path, dqflag=['OK']).getDataset()

j.backend = Dirac()

#j.outputfiles = [LocalFile('*.root')]
j.outputfiles = [DiracFile('*.root'), LocalFile('stdout')]  # grid files

#j.backend.settings['BannedSites'] = ['LCG.CSCS.ch']

print(j)

j.submit()
