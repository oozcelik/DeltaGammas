from Configurables import DaVinci

DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().DataType = "2016"
DaVinci().TupleFile = "Ntuple.root"
DaVinci().InputType = "DST"
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().DDDBtag = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"