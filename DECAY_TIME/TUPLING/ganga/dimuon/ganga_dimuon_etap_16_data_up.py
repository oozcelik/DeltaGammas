splitter = SplitByFiles()
#splitter.maxFiles = 100000
splitter.filesPerJob = 5
splitter.bulksubmit = False
splitter.ignoremissing = True

j = Job(name="16-UP-DATA", splitter=splitter, comment="v1.9.1")

myApp = GaudiExec()
#myApp.directory = "./DaVinciDev_v45r2"
myApp.directory = "/afs/cern.ch/work/p/petruccs/lhcb/analysis/DaVinci/DaVinciDev_v45r2"

j.application = myApp
j.application.platform = "x86_64-centos7-gcc8-opt"
j.application.options = ["options_dimuon_etap_16_data.py", "../../main.py"]

j.inputfiles = [
    "../../decaytotuple.py",
    "../../prodtools.py",
    # f0 stuff
    "../../combiners/F02PiPi.py",
    "../../combiners/JpsiF02PiPi.py",
    # Etap stuff
    "../../combiners/Rho02PiPi.py",
    "../../combiners/Etap2Rho0Gamma.py",
    "../../combiners/JpsiEtap2Rho0G.py"
]

path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST"

j.inputdata = BKQuery(path, dqflag=['OK']).getDataset()

j.backend = Dirac()

#j.outputfiles = [LocalFile('*.root')] # afs files
j.outputfiles = [DiracFile('*.root'), LocalFile('stdout')]  # grid files

#j.backend.settings['BannedSites'] = ['LCG.RAL.uk']

print(j)

j.submit()