splitter = SplitByFiles()
splitter.maxFiles = 100000
splitter.filesPerJob = 5
splitter.bulksubmit = False
splitter.ignoremissing = True

j = Job(name = "F016-DN-DATA", splitter = splitter, comment="RAL ban")

myApp = GaudiExec()
myApp.directory = "DaVinciDev_v44r4"
#myApp.directory = "/afs/cern.ch/user/p/petruccs/DaVinciDev_v44r4"
#myApp.directory = "/eos/lhcb/user/p/petruccs/davinci_local/DaVinciDev_v44r4"

j.application = myApp
j.application.platform = "x86_64-centos7-gcc7-opt"
j.application.options = ["options_dimuon_f0_16_data.py", "../../main.py"]

j.inputfiles = ["../../decaytotuple.py",
                "../../prodtools.py",
                # f0 stuff
                "../../combiners/F02PiPi.py",
                "../../combiners/JpsiF02PiPi.py",
                # Etap stuff
                "../../combiners/Rho02PiPi.py",
                "../../combiners/Etap2Rho0Gamma.py",                
                "../../combiners/JpsiEtap2Rho0G.py"
                ]

path = "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/DIMUON.DST"
#, "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/DIMUON.DST"]

j.inputdata = BKQuery(path, dqflag=['OK']).getDataset()

j.backend = Dirac()

#j.outputfiles = [LocalFile('*.root')] # afs files
j.outputfiles = [DiracFile('*.root'), LocalFile('stdout')] # grid files

j.backend.settings['BannedSites'] = ['LCG.RAL.uk']

print(j)

j.submit()