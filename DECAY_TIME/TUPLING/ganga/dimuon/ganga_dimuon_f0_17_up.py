splitter = SplitByFiles()
splitter.maxFiles = 100000
splitter.filesPerJob = 20
splitter.bulksubmit = False
splitter.ignoremissing = True

j = Job( name = "F017-UP", splitter = splitter, comment="")

myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/user/p/petruccs/DaVinciDev_v44r4"
#myApp.directory = "/eos/lhcb/user/p/petruccs/davinci_local/DaVinciDev_v44r4"

j.application = myApp
j.application.platform = "x86_64-centos7-gcc7-opt"
j.application.options = ["options_dimuon_f0_17_up.py", "../../main.py"]

j.inputfiles = ["../../decaytotuple.py" ,
                "../../prodtools.py" ,
                "../../combiners/F02PiPi.py",
                "../../combiners/JpsiF02PiPi.py",
                # Etap stuff
                "../../combiners/Rho02PiPi.py",
                "../../combiners/JpsiRho0.py",
                "../../combiners/Etap2Rho0Gamma.py",                
                "../../combiners/JpsiEtap2Rho0G.py"
                ]

path = "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/13144031/ALLSTREAMS.DST"
j.inputdata = BKQuery(path, dqflag=['OK']).getDataset()

j.backend = Dirac()

j.outputfiles = [LocalFile('*.root')]

j.backend.settings['BannedSites'] = ['LCG.CSCS.ch']

print(j)

j.submit()
