from Configurables import DaVinci, CondDB

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters(
    HLT2_Code=
    '''HLT_PASS_RE('Hlt2DiMuonDetachedJPsiDecision|Hlt2DiMuonJPsiDecision')''',
    STRIP_Code=
    '''HLT_PASS_RE('StrippingFullDSTDiMuonJpsi2MuMuDetachedLineDecision')''',
)

DaVinci().EventPreFilters = fltrs.filters('Filters')
DaVinci().Simulation = False
DaVinci().EvtMax = -1
DaVinci().DataType = "2016"
DaVinci().TupleFile = "Ntuple.root"
DaVinci().InputType = "DST"
DaVinci().Lumi = not DaVinci().Simulation

CondDB(LatestGlobalTagByDataType=DaVinci().DataType)
