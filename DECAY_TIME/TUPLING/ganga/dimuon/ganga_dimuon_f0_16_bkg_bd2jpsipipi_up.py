splitter = SplitByFiles()
#splitter.maxFiles = 100000
splitter.filesPerJob = 1
splitter.bulksubmit = False
splitter.ignoremissing = True

j = Job(name="F016-UP-MC",
        splitter=splitter,
        comment="f0-bd2jpsipipi - v1.9.1")

myApp = GaudiExec()
#myApp.directory = "./DaVinciDev_v45r2"
myApp.directory = "/afs/cern.ch/work/p/petruccs/lhcb/analysis/DaVinci/DaVinciDev_v45r2"

j.application = myApp
j.application.platform = "x86_64-centos7-gcc8-opt"
j.application.options = [
    "options_dimuon_f0_16_bkg_bd2jpsipipi_up.py", "../../main.py"
]

j.inputfiles = [
    "../../decaytotuple.py",
    "../../prodtools.py",
    # f0 stuff
    "../../combiners/F02PiPi.py",
    "../../combiners/JpsiF02PiPi.py",
    # Etap stuff
    "../../combiners/Rho02PiPi.py",
    "../../combiners/Etap2Rho0Gamma.py",
    "../../combiners/JpsiEtap2Rho0G.py"
]

path = "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x6139160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/11144061/ALLSTREAMS.DST"

j.inputdata = BKQuery(path, dqflag=['OK']).getDataset()

j.backend = Dirac()

#j.outputfiles = [LocalFile('*.root')] # afs files
j.outputfiles = [DiracFile('*.root'), LocalFile('stdout')]  # grid files

#j.backend.settings['BannedSites'] = ['LCG.SARA.nl']

print(j)

j.submit()