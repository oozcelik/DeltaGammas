from Configurables import DaVinci

DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().DataType = "2012"
DaVinci().TupleFile = "Ntuple.root"
DaVinci().InputType = "DST"
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().DDDBtag = "dddb-20170721-2"
DaVinci().CondDBtag = "sim-20160321-2-vc-mu100"