from Configurables import DaVinci

DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().DataType = "2016"
DaVinci().TupleFile = "Ntuple.root"
DaVinci().HistogramFile = "Nhisto.root"
DaVinci().InputType = "DST"
DaVinci().Lumi = not DaVinci().Simulation

# Etap config
DaVinci().DDDBtag = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
DaVinci().Input = [
    "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00094593/0000/00094593_00000141_7.AllStreams.dst"
]
#DATA
#DaVinci().Input = [
#        "/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/DIMUON.DST/00069529/0001/00069529_00017304_1.dimuon.dst"]
#        "/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/DIMUON.DST/00069529/0001/00069529_00017687_1.dimuon.dst",
#        "/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/DIMUON.DST/00069529/0001/00069529_00016355_1.dimuon.dst"]

# F0 config
#DaVinci().DDDBtag = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20190430-vc-md100"
#DaVinci().Input = [
#    "/eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00094223/0000/00094223_00000570_7.AllStreams.dst"]
#    "/eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00094223/0000/00094223_00000955_7.AllStreams.dst"
#]

# f0 background bdjpsipipi to check file, maybe right path, wrong file
#DaVinci().DDDBtag = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
#DaVinci().Input = [
#    "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00087906/0000/00087906_00000613_7.AllStreams.dst"
#]

# f0 background bsjpsikk to check file, maybe right path, wrong file
#DaVinci().DDDBtag = "dddb-20150724"
#DaVinci().CondDBtag = "sim-20161124-2-vc-mu100"
#DaVinci().Input = [
#    "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00058814/0000/00058814_00001356_7.AllStreams.dst"
#]

# f0 background bsjpsphi2pipipi to check file, maybe right path, wrong file
#DaVinci().DDDBtag = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
#DaVinci().Input = [
#    "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00075485/0000/00075485_00000363_7.AllStreams.dst"
#]

# f0 background bdjpsikpi
#DaVinci().DDDBtag = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
#DaVinci().Input = [
#    "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00098752/0000/00098752_00000031_7.AllStreams.dst"
#]

# f0 background bsjpsiphikk
#DaVinci().DDDBtag = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20170721-2-vc-mu100"
#DaVinci().Input = [
#    "/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00068334/0000/00068334_00000194_7.AllStreams.dst"
#]