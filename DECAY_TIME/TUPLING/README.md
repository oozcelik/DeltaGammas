# WGP

Scripts for running a working-group production of JpsiX0 tuples

Test with:

lb-run DaVinci/latest gaudirun.py options.py main.py

### ATTENTION:
If you want to run on ganga, change the DaVinci path.

where main.py reads:
- the list of decays to tuple from decaytotuple.py
- the tuple tool configurations from prodtools.py
- the decay selections and mvas from python files (e.g. JpsiK.py)

and proceeds as:
- particle containers from stripping, and filter
- particle combiners
- particle tupling
- gaudi sequence
