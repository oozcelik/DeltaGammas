


double evaluate(double g, double dg, double t1, double t2){

  // std::cout << g << " " << dg << " " << t1 << " " << t2 << std::endl;  
  double v1 = g-0.5*dg;
  double v2 = g+0.5*dg;
  double fact = v1/v2;
  
  double top = exp(-v2*t2) - exp(-v2*t1);
  double bottom = exp(-v1*t2) - exp(-v1*t1);    
  return fact*top/bottom;
}

#include "Math/IFunction.h"
#include "Minuit2/Minuit2Minimizer.h"
#include <vector>
#include <iostream>

TH1F* g_histo;

double myfcn(const double* x) {

  double chi2=0;
  for (int i =1; i<= g_histo->GetNbinsX(); ++i){
    double val =  g_histo->GetBinContent(i);
    double s2 = g_histo->GetBinError(i)*g_histo->GetBinError(i);
    double expected = x[2]*evaluate(x[0],x[1],g_histo->GetBinLowEdge(i),g_histo->GetBinLowEdge(i) + g_histo->GetBinWidth(i));
    chi2 += std::pow(expected-val,2)/s2;
  }
  // std::cout << "chi2 " << chi2 << std::endl;
  return chi2;
}


void fitToy(int numbins){

  TFile* file = new TFile("toys.root");
  TRandom ran;

  RooDataSet* tset = (RooDataSet*) file->Get("heavy_0") ;
  std::vector<double> vals;
  for (auto i = 0; i < tset->numEntries(); ++i) {
    const RooArgSet* row = tset->get(i);
    vals.push_back(row->getRealValue("t"));
  }
  
  MakeBins tbins(vals,numbins);
  std::vector<float> hbins;
  hbins.push_back(1);
  for (auto i =1 ; i < tbins.size(); ++i){
    hbins.push_back(tbins.binStart(i));
  }
  hbins.push_back(10);

  for (auto i = 0; i < hbins.size(); ++i){
    std::cout << "hbins " <<  hbins[i] <<std::endl;
  }

  // return;
  
  // set up the fitter  
  ROOT::Math::Minimizer* min = new ROOT::Minuit2::Minuit2Minimizer() ;
  min->SetMaxFunctionCalls(1000000);
  min->SetTolerance(0.000001);
  min->SetPrintLevel(0);

  ROOT::Math::Functor f(&myfcn,3);
  double variable[3] = { 0.66,0.085, 0.17};
  double step[3] = {0.01,0.00001,0.0001};
  min->SetFunction(f);

  // set up the binning and histograms
  // const int nbin = 6;
  // float bins[nbin] = {1,1.5, 2, 3, 5,10};
 
  TH1F* bias = new TH1F("bias", "bias" , 100, -0.05, 0.05); bias->Sumw2();
  TH1F* pull = new TH1F("pull", "pull" , 100, -5.0, 5.0); pull->Sumw2();
  TH1F* error = new TH1F("err", "err" , 100, 0.005, 0.015); error->Sumw2();
  
  
  for (int i = 0; i < 1000; ++i){

    int set1= ran.Integer(500);
    int set2= ran.Integer(500);
    
    std::string name1 = "heavy_" + std::to_string(set1); std::string name2 = "light_" + std::to_string(set2);
    RooDataSet* dset = (RooDataSet*) file->Get(name1.c_str()) ;
    RooDataSet* dset2 = (RooDataSet*) file->Get(name2.c_str()) ;

    //   h1->Clear(); h2->Clear();  // h1->Sumw2(); h2->Sumw2();
    // fill histograms

    TH1F* h1 = new TH1F("h","h", hbins.size()-1, &hbins[0]); h1->Sumw2();
    TH1F* h2 = new TH1F("h2","h2", hbins.size()-1, &hbins[0]);  h2->Sumw2();

    for (auto i = 0; i < dset->numEntries(); ++i) {
      const RooArgSet* row = dset->get(i);
      double time  = row->getRealValue("t");
      h1->Fill(time);
    }

    for (auto i = 0; i < dset2->numEntries(); ++i) {
      const RooArgSet* row = dset2->get(i);
      double time  = row->getRealValue("t");
      h2->Fill(time);
    }

    TH1F* h3 = (TH1F*)h2->Clone("div"); //h3->Sumw2();
    h3->Divide(h1);
    h3->Draw();
    g_histo = h3;

    min->SetVariable(0,"g",variable[0], step[0]);
    min->SetLimitedVariable(1,"dg",variable[1], step[1], 0.01, 0.15);
    min->SetLimitedVariable(2,"N",variable[2], step[2], 0.05, 0.25);
    min->FixVariable(0);
    
    min->Minimize();
    double val = min->X()[1] -0.085;
    double err = min->Errors()[1];

    bias->Fill(val);
    pull->Fill(val/err);
    error->Fill(err);
    
    delete h3; delete h2; delete h1;
  } // loop toy


  TCanvas* can = new TCanvas("can1","can1", 800, 600);
  bias->Fit("gaus");
  TF1* fun = bias->GetFunction("gaus");
  bias->Draw("HISTO");
  fun->SetLineColor(2);
  fun->Draw("SAME");
 
    
  TCanvas* can2 = new TCanvas("can2","can2", 800, 600);

  pull->Fit("gaus");
  TF1* fun2 = pull->GetFunction("gaus");
  pull->Draw("HISTO");
  fun2->SetLineColor(2);
  fun2->Draw("SAME");
  
  TCanvas* can3 = new TCanvas("can3","can3", 800, 600);
  error->Draw("HISTO");
}
