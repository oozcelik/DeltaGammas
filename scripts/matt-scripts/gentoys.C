
#include "RooDCBShape.h"

using namespace RooFit;

RooDataSet* gentoys(int nBs , int nBack, double tauback, double min_mass = 5200, double max_mass = 5500){

    RooRealVar m("Bs_DTF_M", "m", min_mass, max_mass);

    // B_s peak
    RooRealVar bs_n_r("bd_n_r", "bd_n_r", 17.2);
    RooRealVar bs_n_l("bd_n_l", "bd_n_l",  7.52);
    RooRealVar bs_alpha_r("bd_alpha_r", "bd_alpha_r", 1.17);
    RooRealVar bs_alpha_l("bd_alpha_l", "bd_alpha_l", -1.25);
    RooRealVar bs_mean("bs_mean", "bs_mean", 5366.89);
    RooRealVar bs_sigma("bs_sigma", "bs_sigma", 10.5, 0, 18);
    RooDCBShape bs_dcb("bs_dcb", "bs_dcb", m, bs_mean, bs_sigma, bs_alpha_l, bs_alpha_r, bs_n_l, bs_n_r);

    RooRealVar taub("taub", "taub", tauback);
    RooExponential bkg("exp", "exp", m, taub);

    RooRealVar bs_yield("bs_yield", "bs_yield", nBs);
    RooRealVar bkg_yield("bkg_yield", "bkg_yield", nBack);

    RooRealVar gauss_phi23pi_mean("gauss_phi23pi_mean", "gauss_phi23pi_mean", 5267.5, 5200, 5650);
    RooRealVar gauss_phi23pi_sigma_left("gauss_phi23pi_sigma_left", "gauss_phi23pi_sigma_left", 108.0, 0, 200);
    RooRealVar gauss_phi23pi_sigma_right("gauss_phi23pi_sigma_right", "gauss_phi23pi_sigma_right", 59.3, 0, 200);
    RooBifurGauss bs2jpsiphi23pi_bigauss("bs2jpsiphi23pi_bigauss", "bs2jpsiphi23pi_bigauss(x, mean, sigmaL, sigmaR)", m, gauss_phi23pi_mean, gauss_phi23pi_sigma_left, gauss_phi23pi_sigma_right);
    RooRealVar phi_yield("phi_yield", "phi_yield", nBs*0.387);
    
    RooAddPdf model("model", "model", RooArgList(bs_dcb,bkg, bs2jpsiphi23pi_bigauss ), RooArgList(bs_yield, bkg_yield,phi_yield));

    RooDataSet* dset = model.generate(m, 0, false, true,"",true,true);
    return dset;    
}

RooDataSet* gentoysCheb(int nBs , int nBack, double tauback, double min_mass = 5200, double max_mass = 5500){

    RooRealVar m("Bs_DTF_M", "m", min_mass, max_mass);

    // B_s peak
    RooRealVar bs_n_r("bd_n_r", "bd_n_r", 17.2);
    RooRealVar bs_n_l("bd_n_l", "bd_n_l",  7.52);
    RooRealVar bs_alpha_r("bd_alpha_r", "bd_alpha_r", 1.17);
    RooRealVar bs_alpha_l("bd_alpha_l", "bd_alpha_l", -1.25);
    RooRealVar bs_mean("bs_mean", "bs_mean", 5366.89);
    RooRealVar bs_sigma("bs_sigma", "bs_sigma", 10.5, 0, 18);
    RooDCBShape bs_dcb("bs_dcb", "bs_dcb", m, bs_mean, bs_sigma, bs_alpha_l, bs_alpha_r, bs_n_l, bs_n_r);

    RooRealVar taub("taub", "taub", tauback);
    RooChebychev bkg("cheb", "cheb", m, taub);

    RooRealVar bs_yield("bs_yield", "bs_yield", nBs);
    RooRealVar bkg_yield("bkg_yield", "bkg_yield", nBack);

    RooRealVar gauss_phi23pi_mean("gauss_phi23pi_mean", "gauss_phi23pi_mean", 5267.5, 5200, 5650);
    RooRealVar gauss_phi23pi_sigma_left("gauss_phi23pi_sigma_left", "gauss_phi23pi_sigma_left", 108.0, 0, 200);
    RooRealVar gauss_phi23pi_sigma_right("gauss_phi23pi_sigma_right", "gauss_phi23pi_sigma_right", 59.3, 0, 200);
    RooBifurGauss bs2jpsiphi23pi_bigauss("bs2jpsiphi23pi_bigauss", "bs2jpsiphi23pi_bigauss(x, mean, sigmaL, sigmaR)", m, gauss_phi23pi_mean, gauss_phi23pi_sigma_left, gauss_phi23pi_sigma_right);
    RooRealVar phi_yield("phi_yield", "phi_yield", nBs*0.387);
    
    RooAddPdf model("model", "model", RooArgList(bs_dcb,bkg, bs2jpsiphi23pi_bigauss ), RooArgList(bs_yield, bkg_yield,phi_yield));

    
    RooDataSet* dset = model.generate(m, 0, false, true,"",true,true);
    return dset;    
}


void makeRun(int ibin,std::string model = "exp"){

  std::string filename = "toys16_bin" + std::to_string(ibin) + "_" + model + ".root";

  // 2016
  
  std::vector<double> sigyields = {2.6306e+02, 2.280e+02, 2.850e+02, 2.2544e+02, 3.13e+02, 1.9320e+02, 2.6215e+02, 2.44e+02};
  std::vector<double> byields = {2381, 1104, 892, 540, 424, 202, 148, 79};
  std::vector<double> taus ={-2.65e-03, -3.3e-03, -4.3e-03,  -2.97e-03, -5.68e-03, -4.73e-03,  -6.57e-03, -5.3e-03};
  std::vector<double> tausCheb ={-3.94e-01,-3.37e-01, -4.98e-01, -4.9e-01 ,-5.76e-01 ,  -3.56e-01,-8.44e-01, -7.57e-01    };
  


  
  //2017
  /*
  std::vector<double> sigyields = { 2.53e+02, 1.62e+02, 2.66e+02,  2.06e+02, 2.52e+02, 1.76e+02, 2.11e+02,  1.9148e+02};
  std::vector<double> byields = {1884, 973, 731, 396, 311, 167, 167, 113};
  std::vector<double> taus ={-2.9066e-03, -3.5991e-03, -4.8148e-03, -3.8589e-03,  -6.6305e-03, -6.1278e-03, -7.6147e-03 , -7.0854e-03};
  std::vector<double> tausCheb ={-3.9e-01, -4.56e-01, -5.68e-01,  -3.86e-01, -7.36e-01, -5.27e-01,  -8.54e-01,  -6.51e-01};
  */
  
  //2018
  /*
  std::vector<double> sigyields = {2.890e+02,  2.17e+02, 3.14e+02 , 2.56e+02, 3.28e+02, 2.15e+02, 2.661e+02, 2.690e+02 };
  std::vector<double> byields = { 2.198e+03, 1.129e+03, 8.49e+02,  4.99e+02, 4.35e+02,  2.41e+02, 1.84e+02, 82.0};
  std::vector<double> taus ={-3.6426e-03, -3.6702e-03,  -3.9496e-03, -4.7589e-03, -5.7291e-03, -5.3012e-03, -5.5546e-03,  -8.0047e-03};
  std::vector<double> tausCheb ={ -4.91e-01, -4.62e-01, -4.8e-01, -5.35e-01,  -6.3e-01,  -5.72e-01, -5.4e-01, -7.63e-01};
  */
  RooRandom::randomGenerator()->SetSeed(201516);
  
  //2012
  /*
  std::vector<double> sigyields = {1.6811e+02, 1.6504e+02,  2.11e+02, 1.54e+02, 2.05e+02,  1.59e+02, 1.70e+02, 1.82e+02 };
  std::vector<double> byields = {1272, 617, 545, 295, 253, 115, 98, 37};
  std::vector<double> taus ={-3.4e-03, -2.8e-03, -4.45e-03,  -3.59e-03, -3.59e-03,  -4.86e-03,  -4.07e-03, -3.43e-03};
  std::vector<double> tausCheb ={-4.91e-01,-4.4e-01,-6.42e-01, -5.76e-01, -5.95e-01, -7.26e-01, -7.70e-01, -7.76e-01};
  */
  
  TFile* file = new TFile(filename.c_str(),"RECREATE");
  for (int i =0; i < 1000; ++i){
    if (model == "exp"){
      RooDataSet* dset = gentoys(sigyields[ibin],byields[ibin],taus[ibin]);
      std::string dname = "dset_" + std::to_string(i);
      dset->SetName(dname.c_str());
      dset->Write();
    }
    else {
      RooDataSet* dset = gentoysCheb(sigyields[ibin],byields[ibin],tausCheb[ibin]);
      std::string dname = "dset_" + std::to_string(i);
      dset->SetName(dname.c_str());
      dset->Write();
    }
  }
  RooRealVar true_yield("true_yield", "true_yield", sigyields[ibin]);
  true_yield.Write();
  file->Close();
}


void makeRuns(std::string model = "exp"){
 
  for (int i =0; i< 8;++i){
    makeRun(i,model);
  }
}
