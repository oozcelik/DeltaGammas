void generate_binned_tuples(std::string input_name = "2015-2016_etap_data_final.root", std::string dir ="2015-2016" )
{
    std::vector<float> bin_start = {0.5, 0.7, 0.9, 1.2, 1.5, 2.0, 2.5, 3.5};
    std::vector<float> bin_end = {0.7, 0.9, 1.2, 1.5, 2.0, 2.5, 3.5, 10.};
    std::vector<std::string> trailer = { "_05-07.root", "_07-09.root", "_09-12.root", "_12-15.root", "_15-20.root", "_20-25.root", "_25-35.root","_35-10.root" };

    for (int i = 0; i < bin_start.size(); ++i){
      std::string cutstring = "mass > 5200 && mass < 5500 && time > " + std::to_string(bin_start[i]) + " && time < " + std::to_string(bin_end[i]);
      std::cout << cutstring << std::endl;
      Cutter(input_name, "DecayTree",trailer[i],dir,cutstring);
    }

}

void makefiles(){

  std::vector<std::string> vec = {"2011-2012_etap_data_final.root","2015-2016_etap_data_final.root","2017_etap_data_final.root", "2018_etap_data_final.root"};
  std::vector<std::string> dirs = {"2011-2012","2015-2016","2017","2018"};
  for (int i=0;i<vec.size(); ++i){
    generate_binned_tuples(vec[i],dirs[i]);
  }
  
}
