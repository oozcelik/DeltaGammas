#!/usr/bin/env python
import json
import uproot, pandas
import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier
# this wrapper makes it possible to train on subset of features
from rep.estimators import SklearnClassifier
#from hep_ml.commonutils import train_test_split
from rep.metaml import ClassifiersFactory
from hep_ml import uboost, gradientboosting as ugb, losses
from functions import *
import math
import sys

try:
    from customMessage import *
    send_message = True
except:
    print("[WARNING] Email library not found")
    send_message = False

max_file = True

local_dir = "./"
if max_file:
    paths = Path("./uboost_max.json")
    variables = Variables("./uboost_max.json", local_dir)
    config_file = "/eos/home-p/petruccs/lhcb/analysis/bs_jpsi_etap/uboost/training_config_max.json"
else:
    paths = Path("./uboost.json")
    variables = Variables("./uboost.json", local_dir)
    config_file = "/eos/home-p/petruccs/lhcb/analysis/bs_jpsi_etap/uboost/training_config.json"

# Getting the branches to use
with open(config_file) as f:
    labels_dict = json.load(f)
#TODO: to change filename for multiple years
if variables.run_on_mc:
    if max_file:
        real_data = [x + "_max_mc_bzero_jpsi_etaprime_prel_info_vars_add_cuts_info_training.root" for x in variables.years_mc]
        out_root = [x + "_mc_out.root" for x in variables.years_mc]
    else:
        print("Not using old data anymore")
        sys.exit(1)
else:
    if max_file:
        real_data = [x + "_max_bzero_jpsi_etaprime_prel_info_vars_add_cuts_info_training.root" for x in variables.years_data]
        out_root = [x + "_data_out.root" for x in variables.years_data]
    else:
        print("Not using old data anymore")
        sys.exit(1)

print("[INFO] Running with the following parameters:")
print("[INFO] Signal: ", paths.signal)
print("[INFO] Background: ", paths.bkg)
print("[INFO] Data: ", paths.data_dir, real_data)
print("[INFO] Training on: ", labels_dict["training"])
print("[INFO] Full data set: ", labels_dict["full"])

if variables.run_on_mc:
    print("[WARNING] Running on MC data")

# Doing stuff
classifiers = ClassifiersFactory()
if variables.generate_classifier:
    X_tree = uproot.open(paths.signal)[variables.tree_name]
    Y_tree = uproot.open(paths.bkg)[variables.tree_name]

    X = X_tree.arrays(branches=labels_dict["full"], outputtype=pandas.DataFrame)
    Y = Y_tree.arrays(branches=labels_dict["full"], outputtype=pandas.DataFrame)

    if max_file:
        x_ctau_list = [item[0] for item in X_tree.arrays(branches="Bs_DTF_ctau")[b'Bs_DTF_ctau']]
        y_ctau_list = [item[0] for item in Y_tree.arrays(branches="Bs_DTF_ctau")[b'Bs_DTF_ctau']]

        X["Bs_DTF_ctau"] = x_ctau_list
        Y["Bs_DTF_ctau"] = y_ctau_list

        x_bs_dtf_m = [item[0] for item in X_tree.arrays(branches="Bs_DTF_M")[b'Bs_DTF_M']]
        y_bs_dtf_m = [item[0] for item in Y_tree.arrays(branches="Bs_DTF_M")[b'Bs_DTF_M']]

        X["Bs_DTF_M"] = x_bs_dtf_m
        Y["Bs_DTF_M"] = y_bs_dtf_m

#        print("[WARNING] Removed log")
#        x_log_chi2 = [math.log(item[0]) for item in X_tree.arrays(branches="Bs_DTF_chi2")[b'Bs_DTF_chi2']]
#        y_log_chi2 = [math.log(item[0]) for item in Y_tree.arrays(branches="Bs_DTF_chi2")[b'Bs_DTF_chi2']]
#
## WARNING: from now on Bs_DTF_chi2 -> log(Bs_DTF_chi2)
#        print("[WARNING] Bs_DTF_chi2 is converted to log of itself")
#        X["Bs_DTF_chi2"] = x_log_chi2
#        Y["Bs_DTF_chi2"] = y_log_chi2

    len_x = len(X.index)
    len_y = len(Y.index)

# TODO: to remove following prints
    print("[DEBUG] Signal has", len_x, "events")
    print("[DEBUG] Bkg has", len_y, "events")

    if len_x > len_y:
        X = X.sample(n=len_y).reset_index(drop=True)
        print("[INFO] Shrinking the signal array")
    elif len_y > len_x:
        Y = Y.sample(n=len_x).reset_index(drop=True)
        print("[INFO] Shrinking the background array")
    else:
        print("[INFO] The two arrays have the same lenght")

# Cleaning NaN
    pandas.options.mode.use_inf_as_na = True
    X = X.dropna()
    Y = Y.dropna()

# Creating the label df
    print("[DEBUG] Creating dataset")
    true_array = np.ones(len(X), dtype=int)
    false_array = np.zeros(len(Y), dtype=int)

    temp_labels = pandas.DataFrame(true_array)
    labels = temp_labels.append(pandas.DataFrame(false_array))

    data = X.append(Y)

    print("[DEBUG] Splitting dataset")
    X_train, X_test, Y_train, Y_test = train_test_split(data, labels, random_state=42)
# Building up the classifier
    base_tree = DecisionTreeClassifier(max_depth=variables.tree_depth)
    uboost_clf = uboost.uBoostClassifier(uniform_features=labels_dict["uniform"],
                                         uniform_label=1,
                                         base_estimator=base_tree,
                                         n_estimators=variables.n_estimators,
                                         train_features=labels_dict["training"],
                                         n_threads=variables.n_threads,
                                         efficiency_steps=variables.efficiency_steps)
    classifiers['uBoost'] = SklearnClassifier(uboost_clf)

    print("[DEBUG] Fitting")
    classifiers.fit(X_train, Y_train[0])

# Saving the classifier
    from joblib import dump, load
    print("[INFO] Saving classifiers in ", variables.folder)
    dump(classifiers, variables.folder + "output_uboost.joblib")
    print("[INFO] Saving test and train")
    X_train.to_pickle(variables.folder + "xtrain.pkl")
    Y_train.to_pickle(variables.folder + "ytrain.pkl")
    X_test.to_pickle(variables.folder + "xtest.pkl")
    Y_test.to_pickle(variables.folder + "ytest.pkl")

if not variables.generate_classifier:
    from joblib import dump, load
    print("[INFO] Reading from: " + variables.folder)
    classifiers = load(variables.folder + "output_uboost.joblib")
    X_train = pandas.read_pickle(variables.folder + "xtrain.pkl")
    Y_train = pandas.read_pickle(variables.folder + "ytrain.pkl")
    X_test = pandas.read_pickle(variables.folder + "xtest.pkl")
    Y_test = pandas.read_pickle(variables.folder + "ytest.pkl")

if variables.generate_plots:
    from rep.report.metrics import RocAuc
    report = classifiers.test_on(X_test, Y_test[0])
    report.learning_curve(RocAuc(), steps=1).plot(new_plot=True, grid=True)
    print("[INFO] Saving learning curve")
    plt.savefig(variables.folder + "learning_curve.pdf")

    # Correlation Matrix
    report.features_correlation_matrix().plot(new_plot=True)
    print("[INFO] Saving correlation matrix")
    plt.savefig(variables.folder + "correlation_matrix.pdf")

    # PAY ATTENTION: this function won't work if
    # we use Y_test instead of Y_test[0] in the
    # declaration of report
    report.roc(physics_notion=True).plot(new_plot=True, figsize=[10, 9], grid=True)
    print("[INFO] Saving roc curve")
    plt.savefig(variables.folder + "roc_curve.pdf")

    report.efficiencies(labels_dict["uniform"]).plot(new_plot=True, grid=True)
    plt.savefig(variables.folder + "efficiencies.pdf")

# Prediction
if variables.run_prediction:
    print("[INFO] Prediction")
    for data_file, output_file in zip(real_data, out_root):
        print("[INFO] Processing: ", data_file)
        if variables.run_on_mc:
            data_tree = uproot.open(paths.mc_dir + data_file)[variables.tree_name]
        else:
            data_tree = uproot.open(paths.data_dir + data_file)[variables.tree_name]
        data = data_tree.arrays(branches=labels_dict["full"], outputtype=pandas.DataFrame)
# Fixing the multi-value ctau coming from Max root files
        if max_file:
            ctau_list = [item[0] for item in data_tree.arrays(branches="Bs_DTF_ctau")[b'Bs_DTF_ctau']]
            bs_m_list = [item[0] for item in data_tree.arrays(branches="Bs_DTF_M")[b'Bs_DTF_M']]
#            log_chi2_list = [math.log(item[0]) for item in data_tree.arrays(branches="Bs_DTF_chi2")[b'Bs_DTF_chi2']]
            data["Bs_DTF_ctau"] = ctau_list
            data["Bs_DTF_M"] = bs_m_list
#            data["Bs_DTF_chi2"] = log_chi2_list
        pandas.options.mode.use_inf_as_na = True
        data = data.dropna()
# Running prediction
        proba = classifiers["uBoost"].predict_proba(data)
# Adding uboost column to the dataset
        data["uboost"] = proba.T[1]
# Writing on ROOT file
        import root_pandas
        print("[INFO] Writing on ", variables.folder + output_file)
        data.to_root(variables.folder + output_file, key=variables.tree_name)

if send_message:
    send_email()
