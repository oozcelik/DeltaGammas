import json

class Path(object):
    def __init__(self, config_path):
        with open(config_path) as f:
            config = json.load(f)

        self.signal = config["paths"]["mc"] + config["files"]["signal"]
        self.bkg = config["paths"]["data"] + config["files"]["background"]
        self.work = config["paths"]["working"]
        self.output = self.work
        self.mc_dir = config["paths"]["mc"]
        self.data_dir = config["paths"]["data"]

class Variables(object):
    def __init__(self, config_path, working_dir):
        with open(config_path) as f:
            config = json.load(f)

        self.tree_depth = config["variables"]["tree_depth"]
        self.n_estimators = config["variables"]["n_estimators"]
        self.n_threads = config["variables"]["n_threads"]
        self.efficiency_steps = config["variables"]["efficiency_steps"]
        self.tree_name = config["variables"]["tree"]
        self.years_data = config["variables"]["years_data"]
        self.years_mc = config["variables"]["years_mc"]
        self.generate_classifier = config["operations"]["generate_classifier"]
        self.run_prediction = config["operations"]["run_prediction"]
        self.run_on_mc = config["operations"]["run_on_mc"]
        self.generate_plots = config["operations"]["generate_plots"]

        import os
        import sys
        if self.run_on_mc:
            self.folder = working_dir + "mc_eff_steps" \
                        + str(self.efficiency_steps) + "_nest" \
                        + str(self.n_estimators) + "_depth" \
                        + str(self.tree_depth) + "/"
        else:
            self.folder = working_dir + "data_eff_steps" \
                        + str(self.efficiency_steps) + "_nest" \
                        + str(self.n_estimators) + "_depth" \
                        + str(self.tree_depth) + "/"

        if not os.path.exists(self.folder):
            if not self.generate_classifier:
                print("[ERROR] Trying to read from a not existing folder.")
                print("[INFO] folder: ", self.folder)
                sys.exit()
            os.makedirs(self.folder)
