#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "TMath.h"
#include "TF1.h"
#include "Math/DistFunc.h"
#include "RooTFnBinding.h"
#include "RooWorkspace.h"
#include "RooHistPdf.h"


using namespace RooFit;
using namespace RooStats;

void basicfit()
{

   TFile *f = new TFile("histfile.root", "READ");  
   TH1 *h = (TH1*)f->Get("m1");
 //  h->Draw();


   RooRealVar m("Bs","m", 5100, 5600) ;
   RooRealVar mean("mean", "mean", 5260, 5200, 5540);
   RooRealVar sigmaL("sigmaL", "sigmaL",70, 0., 120);
   RooRealVar sigmaR("sigmaR", "sigmaR", 50., 0., 100);
   RooBifurGauss birf("birf", "gauss(x,mean,sigma)", m, mean, sigmaL, sigmaR);

   RooRealVar a1("a1", "a1",  -0.1, -1., 1.);
   RooExponential background("backx", "backx", m, a1);
   RooRealVar nsig("nsig","#signal events",  0.0, h->GetEntries());
   RooRealVar nbkg("nbkg","#background events", 0., h->GetEntries());
   RooRealVar bkgfrac("bkgfrac", "Fraction of background", 0.04);
   RooAddPdf model("model","g+a",RooArgList(birf, background), RooArgList(nsig, nbkg));

   RooDataHist datahist("hist", "hist", m, h) ;

   RooHistPdf histpdf("histpdf", "histpdf", m, datahist, 1);

   RooDataSet *dh = histpdf.generate(m, h->GetEntries());
   model.fitTo(*dh);

//   RooFitResult* r_sig = model.fitTo(dh,Save(kTRUE),Range("signal")) ;

   RooPlot *xframe = m.frame(100);
   TCanvas * c=new TCanvas("c","c",800,600);
   dh->plotOn(xframe);
   model.paramOn(xframe, Format("NE",FixedPrecision(5)));
   model.plotOn(xframe, Components(background), LineStyle(ELineStyle::kDashed), LineColor(kRed));   
//    model.plotOn(xframe, LineStyle(ELineStyle::kDashed), LineColor(kRed));
   model.plotOn(xframe) ;
//    cout << "Chi2 " << xframe->chiSquare(4) << endl;
    xframe->Draw() ;
    gPad->SetBottomMargin(0.36);

    RooHist *xpull = xframe->pullHist();
    RooPlot *Pullframe = m.frame();

    Pullframe->addPlotable(xpull, "P");

    Pullframe->GetXaxis()->SetTitle("Bs mass [MeV]");
    Pullframe->GetYaxis()->SetNdivisions(205, kTRUE);
    Pullframe->GetYaxis()->SetTickLength(0.083);
    Pullframe->GetYaxis()->SetRangeUser(-5, 5);

    //Add the pad to plot Pull histo
    TPad *pad2 = new TPad("pad2", "", 0, 0, 1, 1.0 - 0.06, 0, 0, 0.);
    pad2->SetTopMargin(1.0 - 0.36);
    pad2->SetFillStyle(0);
    pad2->Draw();
    pad2->cd();

    Pullframe->Draw();
 //   addBlurb(C);


/////Workspace part ////////////

/*   RooWorkspace wsim("wsim") ;
   wsim.factory("Exponential::bkg(mgg[40,400],alpha[-0.01,-10,0])");
   wsim.factory("Gaussian::sig(mgg,mean[125,80,400],width[3,1,10])");

   RooDataHist* hist_sig = wsim.pdf("sig")->generateBinned(*wsim.var("mgg"),50) ;
   RooDataHist* hist_bkg = wsim.pdf("bkg")->generateBinned(*wsim.var("mgg"),10000) ;

   wsim.factory("expr::S('mu*Snom',mu[1.5],Snom[50])") ;
   wsim.factory("SUM::model(S*sig,Bnom[10000]*bkg)") ;

   RooDataHist* hist_data = wsim.pdf("model")->generateBinned(*wsim.var("mgg")) ;

   RooWorkspace w("w") ;
   w.import(*hist_sig,RooFit::Rename("template_sig")) ;
   w.import(*hist_bkg,RooFit::Rename("template_bkg")) ;
   w.import(*hist_data,RooFit::Rename("observed_data")) ;

   w.factory("HistFunc::sig(mgg,template_sig)") ;
   w.factory("HistFunc::bkg(mgg,template_bkg)") ;

   w.factory("binw[0.277]") ; // == 1/(400-30)
w.factory("expr::S('mu*binw',mu[1,-1,6],binw[0.277])") ;
w.factory("expr::B('Bscale*binw',Bscale[0,6],binw)") ;
w.factory("ASUM::model(S*sig,B*bkg)") ;


w.pdf("model")->fitTo(*hist_data) ;
TCanvas* c1 = new TCanvas();
RooPlot* frame = w.var("mgg")->frame() ;
hist_data->plotOn(frame) ;
w.pdf("model")->plotOn(frame) ;
w.pdf("model")->plotOn(frame,RooFit::Components("bkg"),RooFit::LineStyle(kDashed)) ;
frame->Draw() ;
c1->Draw();

RooStats::ModelConfig mc("ModelConfig",&w);
mc.SetPdf(*w.pdf("model"));
mc.SetParametersOfInterest(*w.var("mu"));
//mc.SetNuisanceParameters(RooArgSet(*w.var("mean"),*w.var("width"),*w.var("alpha")));
mc.SetNuisanceParameters(*w.var("Bscale"));
mc.SetObservables(*w.var("mgg"));

w.import(mc);

w.writeToFile("model.root") ;*/
}







