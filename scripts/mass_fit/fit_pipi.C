#include "RooFFTConvPdf.h"
#include "TMath.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooTruthModel.h"
#include "RooExponential.h"
#include "RooResolutionModel.h"
#include "RooGaussModel.h"
#include "RooCBShape.h"
#include "RooAddModel.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooVoigtian.h"
#include "RooArgusBG.h"
#include "RooDecay.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "RooDataHist.h"
#include "TSystem.h"
#include "RooFitResult.h"
#include "TLatex.h"
#include "RooBifurGauss.h"
#include "RooHist.h"
#include <string>
#include <sstream>
#include <utility>

using namespace RooFit ;

typedef struct {

    double value;
    double error;


} ValueWithError;

typedef struct {

    ValueWithError mean;
    ValueWithError sigma; 
    ValueWithError Nsig;
    ValueWithError Nbkg;

} Result;

void fillValueWithError(ValueWithError* val,RooRealVar* var){
    val->value = var->getVal();
    val->error = var->getError();
}

void addBlurb(TCanvas* can){


    TLatex *myLatex = new TLatex(0.5,0.5,"");
    myLatex->SetTextFont(132); 
    myLatex->SetTextColor(1); 
    myLatex->SetTextSize(0.06); 
    myLatex->SetNDC(kTRUE); 
    myLatex->SetTextAlign(11);  
    myLatex->SetTextSize(0.05); 
    myLatex->DrawLatex(0.65, 0.85,"LHCb Internal"); 
    //   myLatex->DrawLatex(0.62, 0.82,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 7 + 8 TeV, L =3 fb^{-1}}}");

}

std::string massCandidates(double minmass, double maxmass, double nbins){

    double bsize = (maxmass - minmass)/nbins;
    std::stringstream ax; ax << "Candidates/(" << bsize << "MeV/#font[12]{c}^{2})" ;
    return ax.str();
}


void fit_pipi(double minmass = 5200, double maxmass = 5450, std::string filename = "all_selected.root", int nbins = 50 ){
//  LHCb template
    gROOT -> ProcessLine( ".x ./mattStyle.C" );
//  ROOT stuff declaration
    TFile* theFile = new TFile(filename.c_str());
    TTree* tree = (TTree*)theFile->Get("DecayTree");
//  Variables
    float mass;
    tree->SetBranchAddress("mjpsipipi", &mass);
    RooRealVar m("m", "m", minmass, maxmass);
    RooDataSet data("mass_BS", "mass_BS", RooArgSet(m));
    int nevent = tree->GetEntries();
    
    for(int i = 0; i < nevent; ++i){
        tree->GetEntry(i);
        m = mass;
        if(mass > minmass && mass < maxmass){
            data.add(RooArgSet(m));   
        }
    }

    int ntot = data.numEntries();
    
    RooRealVar nsigBs("nsigBs", "nsigBs", data.numEntries()*0.17, 0., data.numEntries());
    RooRealVar nsigBd("nsigBd", "nsigBd", data.numEntries()*0.02, 0., data.numEntries());
    RooRealVar nsigBkg("nsigBkg", "nsigBkg", data.numEntries()*0.17, 0., data.numEntries());
    RooRealVar nback("nback", "nback", data.numEntries()*0.8, 0., data.numEntries());
    RooRealVar nsigK("nsigK", "nsigK", data.numEntries()*0.17, 0., data.numEntries());
    cout << "[INFO] Got " << data.numEntries() << " entries" << endl;

//  B_s peak
    RooRealVar mBs("mBs","mBs", 5366.89, 5360, 5375);
    RooRealVar s("sigma", "sigma", 7, 0, 18);
    RooGaussian bsgauss("bsgauss", "gauss(x, mean, sigma)", m, mBs, s);

//  B_d peak
    RooRealVar dm("dm", "dm", 87.2);
    RooFormulaVar mBd("mBd", "mBd", "mBs - dm", RooArgList(mBs, dm));
    RooRealVar rels("rels", "rels", 1);
    RooFormulaVar sBd("sBd", "sBd", "rels*sigma", RooArgList(s, rels));
    RooGaussian bdgauss("bdgauss", "gauss(x, mean, sigma)", m, mBd, sBd);

//  K^star peak - values from the MC
    RooRealVar mKs("mKs","mKs", 5.22937e+03);
    RooRealVar kSLeft("kSLeft", "kSLeft", 2.98407e+01);
    RooRealVar kSRight("kSRight", "kSRight", 1.23459e+01);
    RooBifurGauss kGauss("kGauss", "kGauss(x, mean, sigmaL, sigmaR)", m, mKs, kSLeft, kSRight);
    mKs.setConstant(kTRUE);
    kSLeft.setConstant(kTRUE);
    kSRight.setConstant(kTRUE);

//  Exponential background
    RooRealVar taub("taub", "taub", -0.001, -0.1, 0.1);
    RooExponential bg("back","exp", m, taub);

//  Model and fit
    RooAddPdf model("model", "model", RooArgList(bsgauss, bdgauss, bg, kGauss), RooArgList(nsigBs, nsigBd, nback, nsigK));
    model.fitTo(data, Extended(), NumCPU(2)); 

//  Plotting
    TCanvas *c1 = new TCanvas("c1", "c1", 900, 300);
    //TCanvas *c1 = new TCanvas("c1", "c1", 10, 44, 600, 400);
    RooPlot* mframe = m.frame();

    data.plotOn(mframe, Binning(nbins));
    model.plotOn(mframe, LineColor(2)); 
//    model.plotOn(mframe,Components(argus.GetName()), LineColor(kBlack), LineStyle(2));
//    model.plotOn(mframe, RooFit::Components(bdgauss), LineColor(3));
//    model.plotOn(mframe, RooFit::Components(bsgauss), LineColor(4));
    RooHist* hpull = mframe->pullHist();
    RooPlot* pullFrame = m.frame(Title("Pull Distribution"));
    pullFrame->addPlotable(hpull,"P");
    c1->Divide(1,2);
    c1->cd(1);
    mframe->Draw();
    mframe->SetTitle("");
    TAxis* xachse = mframe->GetXaxis(); TAxis* yachse = mframe->GetYaxis();
//    xachse->SetTitleFont (132);
//    yachse->SetTitleFont (132);
//    xachse->SetLabelFont (132);
//    yachse->SetLabelFont (132); 
//    std::string axstring = massCandidates(minmass, maxmass, nbins);
    // xachse->SetTitle("m(#psi(2S)#pi#pi) [MeV/c^{2}]"); 
//    yachse->SetTitle(axstring.c_str()); 
//    yachse->SetTitleOffset(1.0); 
//    xachse->SetTitleOffset(1.1);
    mframe->Draw();
    addBlurb(c1);
    
    c1->cd(2);
    pullFrame->Draw();
    addBlurb(c1);
}
