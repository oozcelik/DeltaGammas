void time_acceptance_study_cut_pipi()
{
    TString f0_mc_base_path = "/eos/lhcb/user/p/petruccs/data/bzero_jpsi_f0/mc/";
    TString f0_mc_file_name = "2016_mc_bzero_jpsi_f0.root";

    TFile *f0_file = new TFile(f0_mc_base_path + f0_mc_file_name);
    TTree *f0_tree = (TTree *)f0_file->Get("B2JpsiF02PiPi_Tuple/DecayTree");

    TString etap_mc_base_path = "/eos/lhcb/user/p/petruccs/data/bzero_jpsi_etap/mc/";
    TString etap_mc_file_name = "2016_mc_bzero_jpsi_etaprime.root";

    TFile *etap_file = new TFile(etap_mc_base_path + etap_mc_file_name);
    TTree *etap_tree = (TTree *)etap_file->Get("B2JpsiEtap2Rho0G_Tuple/DecayTree");

    TCut presel_cuts = "Bs_DTF_chi2[0] / Bs_DTF_nDOF[0] < 5 && Bs_DTF_ctau[0] / 0.299792458 > 0.3";

    TCut my_jpsi = "mu1_MC_MOTHER_KEY == mu2_MC_MOTHER_KEY && "
                   "abs(mu1_MC_MOTHER_ID) == 443 && "
                   "abs(mu1_MC_GD_MOTHER_ID) == 531 && "
                   "abs(mu1_TRUEID) == 13 && "
                   "abs(mu2_TRUEID) == 13";

    TCut my_gamma = "gamma_TRUEID == 22 && "
                    "abs(gamma_MC_MOTHER_ID) == 331 && "
                    "abs(gamma_MC_GD_MOTHER_ID) == 531";

    TCut my_etap_pions = "abs(pi1_TRUEID) == 211 && "
                         "abs(pi2_TRUEID) == 211 && "
                         "pi1_MC_MOTHER_KEY == pi2_MC_MOTHER_KEY && "
                         "abs(pi1_MC_MOTHER_ID) == 113 && "
                         "abs(pi1_MC_GD_MOTHER_ID) == 331 && "
                         "abs(pi1_MC_GD_GD_MOTHER_ID) == 531 && "
                         "pi1_MC_GD_MOTHER_KEY == gamma_MC_MOTHER_KEY";

    TCut my_f0_pions = "abs(pi1_TRUEID) == 211 && "
                       "abs(pi2_TRUEID) == 211 && "
                       "pi1_MC_MOTHER_KEY == pi2_MC_MOTHER_KEY && "
                       "abs(pi1_MC_MOTHER_ID) == 531";

    TCut hlt_bs_reduced_hlt_cuts = "Bs_Hlt1DiMuonHighMassDecision_TOS && "
                                   "Bs_Hlt2DiMuonDetachedDecision_TOS";

    TCut etap_truth_match = my_jpsi + my_etap_pions + my_gamma;
    TCut f0_truth_match = my_jpsi + my_f0_pions;

    TCut ipchi2_cut = "min(pi1_IPCHI2_OWNPV, pi2_IPCHI2_OWNPV) > 4";
    TCut hard_ipchi2_cut = "min(pi1_IPCHI2_OWNPV, pi2_IPCHI2_OWNPV) > 6";
    TCut light_tau_cut = "Bs_DTF_ctau[0] / 0.299792458 > 0.5"; //ps
    TCut hard_tau_cut = "Bs_DTF_ctau[0] / 0.299792458 > 1";    //ps

    TH1F *f0_ip_chi2_cut_histo = new TH1F("f0_ip_chi2_cut_histo", "Time acceptance ratio - IPCHI2_OWNPV cut", 100, 0, 10);
    f0_ip_chi2_cut_histo->Sumw2();
    TH1F *f0_hard_ip_chi2_cut_histo = new TH1F("f0_hard_ip_chi2_cut_histo", "Time acceptance ratio - hard IPCHI2_OWNPV cut", 100, 0, 10);
    f0_hard_ip_chi2_cut_histo->Sumw2();
    TH1F *f0_light_tau_cut_histo = new TH1F("f0_light_tau_cut_histo", "Time acceptance ratio - 0.5ps cut", 100, 0, 10);
    f0_light_tau_cut_histo->Sumw2();
    TH1F *f0_hard_tau_cut_histo = new TH1F("f0_hard_tau_cut_histo", "Time acceptance ratio - 1ps cut", 100, 0, 10);
    f0_hard_tau_cut_histo->Sumw2();
    TH1F *f0_light_tau_ip_chi2_cut = new TH1F("f0_light_tau_ip_chi2_cut", "Time acceptance ratio - IPCHI2_OWNPV cut + 0.5ps cut", 100, 0, 10);
    f0_light_tau_ip_chi2_cut->Sumw2();
    TH1F *f0_reference_histo = new TH1F("f0_reference_histo", "Time acceptance ratio - no trigger", 100, 0, 10);
    f0_reference_histo->Sumw2();

    TH1F *etap_ip_chi2_cut_histo = new TH1F("etap_ip_chi2_cut_histo", "Time acceptance ratio - IPCHI2_OWNPV cut", 100, 0, 10);
    etap_ip_chi2_cut_histo->Sumw2();
    TH1F *etap_hard_ip_chi2_cut_histo = new TH1F("etap_hard_ip_chi2_cut_histo", "Time acceptance ratio - hard IPCHI2_OWNPV cut", 100, 0, 10);
    etap_hard_ip_chi2_cut_histo->Sumw2();
    TH1F *etap_light_tau_cut_histo = new TH1F("etap_light_tau_cut_histo", "Time acceptance ratio - 0.5ps cut", 100, 0, 10);
    etap_light_tau_cut_histo->Sumw2();
    TH1F *etap_hard_tau_cut_histo = new TH1F("etap_hard_tau_cut_histo", "Time acceptance ratio - 1ps cut", 100, 0, 10);
    etap_hard_tau_cut_histo->Sumw2();
    TH1F *etap_light_tau_ip_chi2_cut = new TH1F("etap_light_tau_ip_chi2_cut", "Time acceptance ratio - IPCHI2_OWNPV cut + 0.5ps cut", 100, 0, 10);
    etap_light_tau_ip_chi2_cut->Sumw2();
    TH1F *etap_reference_histo = new TH1F("etap_reference_histo", "Time acceptance ratio - reference (truth-match + preselection + reduced HLT)", 100, 0, 10);
    etap_reference_histo->Sumw2();

    etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_ip_chi2_cut_histo", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + ipchi2_cut);
    f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_ip_chi2_cut_histo", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + ipchi2_cut);
    etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_hard_ip_chi2_cut_histo", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + hard_ipchi2_cut);
    f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_hard_ip_chi2_cut_histo", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + hard_ipchi2_cut);

    etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_light_tau_cut_histo", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + light_tau_cut);
    f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_light_tau_cut_histo", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + light_tau_cut);

    etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_hard_tau_cut_histo", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + hard_tau_cut);
    f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_hard_tau_cut_histo", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + hard_tau_cut);

    etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_light_tau_ip_chi2_cut", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + ipchi2_cut + light_tau_cut);
    f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_light_tau_ip_chi2_cut", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + ipchi2_cut + light_tau_cut);

    etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_reference_histo", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts);
    f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_reference_histo", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts);

    //// PLOTTING HISTO 1
    //// Normalizing histograms
    Double_t scale_etap_ip_chi2_cut_histo = 1 / etap_ip_chi2_cut_histo->Integral("width");
    etap_ip_chi2_cut_histo->Scale(scale_etap_ip_chi2_cut_histo);
    Double_t scale_f0_ip_chi2_cut_histo = 1 / f0_ip_chi2_cut_histo->Integral("width");
    f0_ip_chi2_cut_histo->Scale(scale_f0_ip_chi2_cut_histo);

    // Scaling histos
    etap_ip_chi2_cut_histo->Divide(f0_ip_chi2_cut_histo);

    // Plotting histograms
    TCanvas *c1 = new TCanvas("c1", "c1", 10, 44, 600, 400);

    etap_ip_chi2_cut_histo->SetMarkerStyle(20);
    etap_ip_chi2_cut_histo->GetXaxis()->SetTitleFont(132);
    etap_ip_chi2_cut_histo->GetYaxis()->SetTitleFont(132);
    etap_ip_chi2_cut_histo->GetXaxis()->SetLabelFont(132);
    etap_ip_chi2_cut_histo->GetYaxis()->SetLabelFont(132);
    etap_ip_chi2_cut_histo->GetXaxis()->SetTitle("#tau [ps]");
    etap_ip_chi2_cut_histo->Draw("PE");
    //
    gStyle->SetOptFit();
    etap_ip_chi2_cut_histo->Fit("pol1", "", "", 1., 10.);

    // PLOTTING HISTO 2
    // Normalizing histograms
    Double_t scale_etap_light_tau_cut_histo = 1 / etap_light_tau_cut_histo->Integral("width");
    etap_light_tau_cut_histo->Scale(scale_etap_light_tau_cut_histo);
    Double_t scale_f0_light_tau_cut_histo = 1 / f0_light_tau_cut_histo->Integral("width");
    f0_light_tau_cut_histo->Scale(scale_f0_light_tau_cut_histo);

    // Scaling histos
    etap_light_tau_cut_histo->Divide(f0_light_tau_cut_histo);

    // Plotting histograms
    TCanvas *c2 = new TCanvas("c2", "c2", 10, 44, 600, 400);

    etap_light_tau_cut_histo->SetMarkerStyle(20);
    etap_light_tau_cut_histo->GetXaxis()->SetTitleFont(132);
    etap_light_tau_cut_histo->GetYaxis()->SetTitleFont(132);
    etap_light_tau_cut_histo->GetXaxis()->SetLabelFont(132);
    etap_light_tau_cut_histo->GetYaxis()->SetLabelFont(132);
    etap_light_tau_cut_histo->GetXaxis()->SetTitle("#tau [ps]");
    etap_light_tau_cut_histo->Draw("PE");
    //
    gStyle->SetOptFit();
    etap_light_tau_cut_histo->Fit("pol1", "", "", 1., 10.);

    // PLOTTING HISTO 3
    // Normalizing histograms
    Double_t scale_etap_hard_tau_cut_histo = 1 / etap_hard_tau_cut_histo->Integral("width");
    etap_hard_tau_cut_histo->Scale(scale_etap_hard_tau_cut_histo);
    Double_t scale_f0_hard_tau_cut_histo = 1 / f0_hard_tau_cut_histo->Integral("width");
    f0_hard_tau_cut_histo->Scale(scale_f0_hard_tau_cut_histo);

    // Scaling histos
    etap_hard_tau_cut_histo->Divide(f0_hard_tau_cut_histo);

    // Plotting histograms
    TCanvas *c3 = new TCanvas("c3", "c3", 10, 44, 600, 400);

    etap_hard_tau_cut_histo->SetMarkerStyle(20);
    etap_hard_tau_cut_histo->GetXaxis()->SetTitleFont(132);
    etap_hard_tau_cut_histo->GetYaxis()->SetTitleFont(132);
    etap_hard_tau_cut_histo->GetXaxis()->SetLabelFont(132);
    etap_hard_tau_cut_histo->GetYaxis()->SetLabelFont(132);
    etap_hard_tau_cut_histo->GetXaxis()->SetTitle("#tau [ps]");
    etap_hard_tau_cut_histo->Draw("PE");
    //
    gStyle->SetOptFit();
    etap_hard_tau_cut_histo->Fit("pol1", "", "", 1., 10.);

    // PLOTTING HISTO 4
    // Normalizing histograms
    Double_t scale_etap_light_tau_ip_chi2_cut = 1 / etap_light_tau_ip_chi2_cut->Integral("width");
    etap_light_tau_ip_chi2_cut->Scale(scale_etap_light_tau_ip_chi2_cut);
    Double_t scale_f0_light_tau_ip_chi2_cut = 1 / f0_light_tau_ip_chi2_cut->Integral("width");
    f0_light_tau_ip_chi2_cut->Scale(scale_f0_light_tau_ip_chi2_cut);

    // Scaling histos
    etap_light_tau_ip_chi2_cut->Divide(f0_light_tau_ip_chi2_cut);

    // Plotting histograms
    TCanvas *c4 = new TCanvas("c4", "c4", 10, 44, 600, 400);

    etap_light_tau_ip_chi2_cut->SetMarkerStyle(20);
    etap_light_tau_ip_chi2_cut->GetXaxis()->SetTitleFont(132);
    etap_light_tau_ip_chi2_cut->GetYaxis()->SetTitleFont(132);
    etap_light_tau_ip_chi2_cut->GetXaxis()->SetLabelFont(132);
    etap_light_tau_ip_chi2_cut->GetYaxis()->SetLabelFont(132);
    etap_light_tau_ip_chi2_cut->GetXaxis()->SetTitle("#tau [ps]");
    etap_light_tau_ip_chi2_cut->Draw("PE");
    //
    gStyle->SetOptFit();
    etap_light_tau_ip_chi2_cut->Fit("pol1", "", "", 1., 10.);

    // PLOTTING HISTO 5
    // Normalizing histograms
    Double_t scale_etap_reference_histo = 1 / etap_reference_histo->Integral("width");
    etap_reference_histo->Scale(scale_etap_reference_histo);
    Double_t scale_f0_reference_histo = 1 / f0_reference_histo->Integral("width");
    f0_reference_histo->Scale(scale_f0_reference_histo);

    // Scaling histos
    etap_reference_histo->Divide(f0_reference_histo);

    // Plotting histograms
    TCanvas *c5 = new TCanvas("c5", "c5", 10, 44, 600, 400);

    etap_reference_histo->SetMarkerStyle(20);
    etap_reference_histo->GetXaxis()->SetTitleFont(132);
    etap_reference_histo->GetYaxis()->SetTitleFont(132);
    etap_reference_histo->GetXaxis()->SetLabelFont(132);
    etap_reference_histo->GetYaxis()->SetLabelFont(132);
    etap_reference_histo->GetXaxis()->SetTitle("#tau [ps]");
    etap_reference_histo->Draw("PE");
    //
    gStyle->SetOptFit();
    etap_reference_histo->Fit("pol1", "", "", 1., 10.);

    // PLOTTING HISTO 6
    // Normalizing histograms
    Double_t scale_etap_hard_ip_chi2_cut_histo = 1 / etap_hard_ip_chi2_cut_histo->Integral("width");
    etap_hard_ip_chi2_cut_histo->Scale(scale_etap_hard_ip_chi2_cut_histo);
    Double_t scale_f0_hard_ip_chi2_cut_histo = 1 / f0_hard_ip_chi2_cut_histo->Integral("width");
    f0_hard_ip_chi2_cut_histo->Scale(scale_f0_hard_ip_chi2_cut_histo);

    // Scaling histos
    etap_hard_ip_chi2_cut_histo->Divide(f0_hard_ip_chi2_cut_histo);

    // Plotting histograms
    TCanvas *c6 = new TCanvas("c6", "c6", 10, 44, 600, 400);

    etap_hard_ip_chi2_cut_histo->SetMarkerStyle(20);
    etap_hard_ip_chi2_cut_histo->SetFillColor(kRed);
    etap_hard_ip_chi2_cut_histo->GetXaxis()->SetTitleFont(132);
    etap_hard_ip_chi2_cut_histo->GetYaxis()->SetTitleFont(132);
    etap_hard_ip_chi2_cut_histo->GetXaxis()->SetLabelFont(132);
    etap_hard_ip_chi2_cut_histo->GetYaxis()->SetLabelFont(132);
    etap_hard_ip_chi2_cut_histo->GetXaxis()->SetTitle("#tau [ps]");
    etap_hard_ip_chi2_cut_histo->Draw("PE, SAME");
    //
    gStyle->SetOptFit();
    etap_hard_ip_chi2_cut_histo->Fit("pol1", "", "", 1., 10.);
}