#include "TMath.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooTruthModel.h"
#include "RooExponential.h"
#include "RooResolutionModel.h"
#include "RooGaussModel.h"
#include "RooCBShape.h"
#include "RooAddModel.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooDecay.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TCut.h"
#include "TFile.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "RooDataHist.h"
#include "TSystem.h"
#include "RooFitResult.h"
#include "TLatex.h"

// TODO: to check include

/*
   FCN=212.622 FROM MIGRAD    STATUS=CONVERGED     126 CALLS         127 TOTAL
   EDM=1.22078e-10    STRATEGY= 1      ERROR MATRIX ACCURATE 
   EXT PARAMETER                                   STEP         FIRST   
   NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  c            5.36594e-03   5.89684e-05   2.77223e-07   6.06864e-02
   2  beta         3.26077e-02   4.18433e-03   1.96712e-05   3.04809e-03
   */
using namespace RooFit;

void time_acceptance_study()
{
   TString etap_mc_base_path = "/eos/lhcb/user/p/petruccs/data/bzero_jpsi_etap/mc/";
   TString f0_mc_base_path = "/eos/lhcb/user/p/petruccs/data/bzero_jpsi_f0/mc/";

   TString etap_mc_file_name = "2016_mc_bzero_jpsi_etaprime_bug_fix.root";
   TString f0_mc_file_name = "2016_mc_bzero_jpsi_f0_beauty_match_partial_truth_matched.root";

   TFile *etap_file = new TFile(etap_mc_base_path + etap_mc_file_name);
   TFile *f0_file = new TFile(f0_mc_base_path + f0_mc_file_name);

   TTree *etap_tree = (TTree *)etap_file->Get("B2JpsiEtap2Rho0G_Tuple/DecayTree");
   TTree *f0_tree = (TTree *)f0_file->Get("DecayTree");

   TCut presel_cuts = "Bs_DTF_chi2[0] / Bs_DTF_nDOF[0] < 5 && Bs_DTF_ctau[0] > 0.1";

   TCut hlt_jpsi_full_cuts = "Jpsi_Hlt1DiMuonHighMassDecision_TOS && "
                             "Jpsi_Hlt2DiMuonDetachedDecision_TOS && "
                             "Jpsi_Hlt1TrackMuonDecision_TOS && "
                             "Jpsi_Hlt1TwoTrackMVADecision_TOS && "
                             "Jpsi_Hlt2DiMuonDetachedJPsiDecision_TOS";
   TCut l0_jpsi_full_cuts = "(Jpsi_L0DiMuonDecision_TOS || Jpsi_L0MuonDecision_TOS)";
   TCut hlt_jpsi_reduced_hlt_cuts = "Jpsi_Hlt1DiMuonHighMassDecision_TOS && "
                                    "Jpsi_Hlt2DiMuonDetachedDecision_TOS";

   TCut hlt_bs_full_cuts = "Bs_Hlt1DiMuonHighMassDecision_TOS && "
                           "Bs_Hlt2DiMuonDetachedDecision_TOS && "
                           "Bs_Hlt1TrackMuonDecision_TOS && "
                           "Bs_Hlt1TwoTrackMVADecision_TOS && "
                           "Bs_Hlt2DiMuonDetachedJPsiDecision_TOS";
   TCut l0_bs_full_cuts = "(Bs_L0DiMuonDecision_TOS || Bs_L0MuonDecision_TOS)";
   TCut hlt_bs_reduced_hlt_cuts = "Bs_Hlt1DiMuonHighMassDecision_TOS && "
                                  "Bs_Hlt2DiMuonDetachedDecision_TOS";

   TCut my_jpsi = "mu1_MC_MOTHER_KEY == mu2_MC_MOTHER_KEY && "
                  "abs(mu1_MC_MOTHER_ID) == 443 && "
                  "abs(mu1_MC_GD_MOTHER_ID) == 531 && "
                  "abs(mu1_TRUEID) == 13 && "
                  "abs(mu2_TRUEID) == 13";

   TCut my_gamma = "gamma_TRUEID == 22 && "
                   "abs(gamma_MC_MOTHER_ID) == 331 && "
                   "abs(gamma_MC_GD_MOTHER_ID) == 531";

   TCut my_etap_pions = "abs(pi1_TRUEID) == 211 && "
                        "abs(pi2_TRUEID) == 211 && "
                        "pi1_MC_MOTHER_KEY == pi2_MC_MOTHER_KEY && "
                        "abs(pi1_MC_MOTHER_ID) == 113 && "
                        "abs(pi1_MC_GD_MOTHER_ID) == 331 && "
                        "abs(pi1_MC_GD_GD_MOTHER_ID) == 531 && "
                        "pi1_MC_GD_MOTHER_KEY == gamma_MC_MOTHER_KEY";

   TCut my_f0_pions = "abs(pi1_TRUEID) == 211 && "
                      "abs(pi2_TRUEID) == 211 && "
                      "pi1_MC_MOTHER_KEY == pi2_MC_MOTHER_KEY && "
                      "abs(pi1_MC_MOTHER_ID) == 531";

   TCut etap_truth_match = my_jpsi + my_etap_pions + my_gamma;
   TCut f0_truth_match = my_jpsi + my_f0_pions;

   TH1F *f0_full_trigger_bs_histo = new TH1F("f0_full_trigger_bs_histo", "Time acceptance ratio - L0 + HLT attached to Bs", 100, 0, 10);
   f0_full_trigger_bs_histo->Sumw2();
   TH1F *f0_l0_bs_histo = new TH1F("f0_l0_bs_histo", "Time acceptance ratio - L0 attached to Bs", 100, 0, 10);
   f0_l0_bs_histo->Sumw2();
   TH1F *f0_l0_reduced_hlt_bs_histo = new TH1F("f0_l0_reduced_hlt_bs_histo", "Time acceptance ratio - L0 + reduced HLT attached to Bs", 100, 0, 10);
   f0_l0_reduced_hlt_bs_histo->Sumw2();
   TH1F *f0_full_hlt_bs_histo = new TH1F("f0_full_hlt_bs_histo", "Time acceptance ratio - full HLT attached to Bs", 100, 0, 10);
   f0_full_hlt_bs_histo->Sumw2();
   TH1F *f0_no_trigger_histo = new TH1F("f0_no_trigger_histo", "Time acceptance ratio - no trigger", 100, 0, 10);
   f0_no_trigger_histo->Sumw2();

   TH1F *etap_full_trigger_bs_histo = new TH1F("etap_full_trigger_bs_histo", "Time acceptance ratio - L0 + HLT attached to Bs", 100, 0, 10);
   etap_full_trigger_bs_histo->Sumw2();
   TH1F *etap_l0_bs_histo = new TH1F("etap_l0_bs_histo", "Time acceptance ratio - L0 attached to Bs", 100, 0, 10);
   etap_l0_bs_histo->Sumw2();
   TH1F *etap_l0_reduced_hlt_bs_histo = new TH1F("etap_l0_reduced_hlt_bs_histo", "Time acceptance ratio - L0 + reduced HLT attached to Bs", 100, 0, 10);
   etap_l0_reduced_hlt_bs_histo->Sumw2();
   TH1F *etap_full_hlt_bs_histo = new TH1F("etap_full_hlt_bs_histo", "Time acceptance ratio - full HLT attached to Bs", 100, 0, 10);
   etap_full_hlt_bs_histo->Sumw2();
   TH1F *etap_no_trigger_histo = new TH1F("etap_no_trigger_histo", "Time acceptance ratio - no trigger", 100, 0, 10);
   etap_no_trigger_histo->Sumw2();

   etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_full_trigger_bs_histo", etap_truth_match + presel_cuts + hlt_bs_full_cuts + l0_bs_full_cuts);
   f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_full_trigger_bs_histo", f0_truth_match + presel_cuts + hlt_bs_full_cuts + l0_bs_full_cuts);

   etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_l0_bs_histo", etap_truth_match + presel_cuts + l0_bs_full_cuts);
   f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_l0_bs_histo", f0_truth_match + presel_cuts + l0_bs_full_cuts);

   etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_l0_reduced_hlt_bs_histo", etap_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + l0_bs_full_cuts);
   f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_l0_reduced_hlt_bs_histo", f0_truth_match + presel_cuts + hlt_bs_reduced_hlt_cuts + l0_bs_full_cuts);

   etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_full_hlt_bs_histo", etap_truth_match + presel_cuts + hlt_bs_full_cuts);
   f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_full_hlt_bs_histo", f0_truth_match + presel_cuts + hlt_bs_full_cuts);

   etap_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> etap_no_trigger_histo", etap_truth_match + presel_cuts);
   f0_tree->Draw("Bs_DTF_ctau[0] / 0.299792458 >> f0_no_trigger_histo", f0_truth_match + presel_cuts);

   // PLOTTING HISTO 1
   // Normalizing histograms
   Double_t scale_etap_full_trigger_bs_histo = 1 / etap_full_trigger_bs_histo->Integral("width");
   etap_full_trigger_bs_histo->Scale(scale_etap_full_trigger_bs_histo);
   Double_t scale_f0_full_trigger_bs_histo = 1 / f0_full_trigger_bs_histo->Integral("width");
   f0_full_trigger_bs_histo->Scale(scale_f0_full_trigger_bs_histo);

   // Scaling histos
   f0_full_trigger_bs_histo->Divide(etap_full_trigger_bs_histo);

   // Plotting histograms
   TCanvas *c1 = new TCanvas("c1", "c1", 10, 44, 600, 400);

   f0_full_trigger_bs_histo->SetMarkerStyle(20);
   f0_full_trigger_bs_histo->GetXaxis()->SetTitleFont(132);
   f0_full_trigger_bs_histo->GetYaxis()->SetTitleFont(132);
   f0_full_trigger_bs_histo->GetXaxis()->SetLabelFont(132);
   f0_full_trigger_bs_histo->GetYaxis()->SetLabelFont(132);
   f0_full_trigger_bs_histo->GetXaxis()->SetTitle("#tau [ps]");
   f0_full_trigger_bs_histo->GetYaxis()->SetTitle("Acceptance ratio");
   f0_full_trigger_bs_histo->Draw("PE");
   //
   gStyle->SetOptFit();
   f0_full_trigger_bs_histo->Fit("pol1", "", "", 1., 10.);

   // PLOTTING HISTO 2
   // Normalizing histograms
   Double_t scale_etap_l0_bs_histo = 1 / etap_l0_bs_histo->Integral("width");
   etap_l0_bs_histo->Scale(scale_etap_l0_bs_histo);
   Double_t scale_f0_l0_bs_histo = 1 / f0_l0_bs_histo->Integral("width");
   f0_l0_bs_histo->Scale(scale_f0_l0_bs_histo);

   // Scaling histos
   f0_l0_bs_histo->Divide(etap_l0_bs_histo);

   // Plotting histograms
   TCanvas *c2 = new TCanvas("c2", "c2", 10, 44, 600, 400);

   f0_l0_bs_histo->SetMarkerStyle(20);
   f0_l0_bs_histo->GetXaxis()->SetTitleFont(132);
   f0_l0_bs_histo->GetYaxis()->SetTitleFont(132);
   f0_l0_bs_histo->GetXaxis()->SetLabelFont(132);
   f0_l0_bs_histo->GetYaxis()->SetLabelFont(132);
   f0_l0_bs_histo->GetXaxis()->SetTitle("#tau [ps]");
   f0_l0_bs_histo->GetYaxis()->SetTitle("Acceptance ratio");
   f0_l0_bs_histo->Draw("PE");
   //
   gStyle->SetOptFit();
   f0_l0_bs_histo->Fit("pol1", "", "", 1., 10.);

   // PLOTTING HISTO 3
   // Normalizing histograms
   Double_t scale_etap_l0_reduced_hlt_bs_histo = 1 / etap_l0_reduced_hlt_bs_histo->Integral("width");
   etap_l0_reduced_hlt_bs_histo->Scale(scale_etap_l0_reduced_hlt_bs_histo);
   Double_t scale_f0_l0_reduced_hlt_bs_histo = 1 / f0_l0_reduced_hlt_bs_histo->Integral("width");
   f0_l0_reduced_hlt_bs_histo->Scale(scale_f0_l0_reduced_hlt_bs_histo);

   // Scaling histos
   f0_l0_reduced_hlt_bs_histo->Divide(etap_l0_reduced_hlt_bs_histo);

   // Plotting histograms
   TCanvas *c3 = new TCanvas("c3", "c3", 10, 44, 600, 400);

   f0_l0_reduced_hlt_bs_histo->SetMarkerStyle(20);
   f0_l0_reduced_hlt_bs_histo->GetXaxis()->SetTitleFont(132);
   f0_l0_reduced_hlt_bs_histo->GetYaxis()->SetTitleFont(132);
   f0_l0_reduced_hlt_bs_histo->GetXaxis()->SetLabelFont(132);
   f0_l0_reduced_hlt_bs_histo->GetYaxis()->SetLabelFont(132);
   f0_l0_reduced_hlt_bs_histo->GetXaxis()->SetTitle("#tau [ps]");
   f0_l0_reduced_hlt_bs_histo->GetYaxis()->SetTitle("Acceptance ratio");
   f0_l0_reduced_hlt_bs_histo->Draw("PE");
   //
   gStyle->SetOptFit();
   f0_l0_reduced_hlt_bs_histo->Fit("pol1", "", "", 1., 10.);

   // PLOTTING HISTO 4
   // Normalizing histograms
   Double_t scale_etap_full_hlt_bs_histo = 1 / etap_full_hlt_bs_histo->Integral("width");
   etap_full_hlt_bs_histo->Scale(scale_etap_full_hlt_bs_histo);
   Double_t scale_f0_full_hlt_bs_histo = 1 / f0_full_hlt_bs_histo->Integral("width");
   f0_full_hlt_bs_histo->Scale(scale_f0_full_hlt_bs_histo);

   // Scaling histos
   f0_full_hlt_bs_histo->Divide(etap_full_hlt_bs_histo);

   // Plotting histograms
   TCanvas *c4 = new TCanvas("c4", "c4", 10, 44, 600, 400);

   f0_full_hlt_bs_histo->SetMarkerStyle(20);
   f0_full_hlt_bs_histo->GetXaxis()->SetTitleFont(132);
   f0_full_hlt_bs_histo->GetYaxis()->SetTitleFont(132);
   f0_full_hlt_bs_histo->GetXaxis()->SetLabelFont(132);
   f0_full_hlt_bs_histo->GetYaxis()->SetLabelFont(132);
   f0_full_hlt_bs_histo->GetXaxis()->SetTitle("#tau [ps]");
   f0_full_hlt_bs_histo->GetYaxis()->SetTitle("Acceptance ratio");
   f0_full_hlt_bs_histo->Draw("PE");
   //
   gStyle->SetOptFit();
   f0_full_hlt_bs_histo->Fit("pol1", "", "", 1., 10.);

   // PLOTTING HISTO 5
   // Normalizing histograms
   Double_t scale_etap_no_trigger_histo = 1 / etap_no_trigger_histo->Integral("width");
   etap_no_trigger_histo->Scale(scale_etap_no_trigger_histo);
   Double_t scale_f0_no_trigger_histo = 1 / f0_no_trigger_histo->Integral("width");
   f0_no_trigger_histo->Scale(scale_f0_no_trigger_histo);

   // Scaling histos
   f0_no_trigger_histo->Divide(etap_no_trigger_histo);

   // Plotting histograms
   TCanvas *c5 = new TCanvas("c5", "c5", 10, 44, 600, 400);

   f0_no_trigger_histo->SetMarkerStyle(20);
   f0_no_trigger_histo->GetXaxis()->SetTitleFont(132);
   f0_no_trigger_histo->GetYaxis()->SetTitleFont(132);
   f0_no_trigger_histo->GetXaxis()->SetLabelFont(132);
   f0_no_trigger_histo->GetYaxis()->SetLabelFont(132);
   f0_no_trigger_histo->GetXaxis()->SetTitle("#tau [ps]");
   f0_no_trigger_histo->GetYaxis()->SetTitle("Acceptance ratio");
   f0_no_trigger_histo->Draw("PE");
   //
   gStyle->SetOptFit();
   f0_no_trigger_histo->Fit("pol1", "", "", 1., 10.);
}
