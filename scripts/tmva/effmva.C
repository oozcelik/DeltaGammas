#include "TPolyLine.h"
#include "TMath.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooResolutionModel.h"
#include "RooGaussModel.h"
#include "RooAddModel.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooDecay.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TFile.h"
#include "RooPlot.h"
#include "TAxis.h"
#include "RooConstVar.h"
#include <string>
#include "RooDataHist.h"
#include "TH1F.h"
#include "TSpline.h"
#include "RooEffProd.h"
#include "TMath.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooTruthModel.h"
#include "RooExponential.h"
#include "RooResolutionModel.h"
#include "RooGaussModel.h"
#include "RooCBShape.h"
#include "RooAddModel.h"
#include "RooAbsPdf.h"
#include "RooAddPdf.h"
#include "RooHistPdf.h"
#include "RooProdPdf.h"
#include "RooDecay.h"
#include "TGraphErrors.h"
#include "TLine.h"
typedef struct{

  TPolyLine lower;
  TPolyLine upper;
  TPolyLine central;
  TPolyLine area;

}Envelope;

Envelope* makeEnvelope(TGraphErrors* prof ){

  Envelope* env = new Envelope();
  int nbin = prof->GetN();
  for (auto i = 0; i < nbin; ++i){  
    env->central.SetNextPoint(prof->GetX()[i], prof->GetY()[i]);
    env->upper.SetNextPoint(prof->GetX()[i], prof->GetY()[i] + prof->GetEY()[i]);
    env->lower.SetNextPoint(prof->GetX()[i],  prof->GetY()[i] - prof->GetEY()[i]) ;
  }


  for (auto i = 0; i < nbin; ++i){
    env->area.SetNextPoint(prof->GetX()[i], prof->GetY()[i] - prof->GetEY()[i]);
  }

  for (auto i = nbin-1; i > 0; --i){
    env->area.SetNextPoint( prof->GetX()[i], prof->GetY()[i] + prof->GetEY()[i]);
  }


  env->area.SetNextPoint( prof->GetX()[0], prof->GetY()[0] - prof->GetEY()[0]);

  env->area.Print();
  return env;
}


void plotGraph(TGraph* graph,  std::string yTitle, int marker, int color, int style = 1 ){

  graph->SetMarkerStyle(marker);
  graph->SetMarkerColor(color);
  graph->SetLineColor(color);
  graph->SetLineStyle(style);
  graph->SetLineWidth(1);
  //  graph->SetMinimum(0.0);
  graph->SetTitle("");
  graph->GetXaxis()->SetTitleFont(132);
  graph->GetYaxis()->SetTitleFont(132);
  graph->GetXaxis()->SetTitleOffset(1.15);
  // graph->GetYaxis()->SetTitleOffset(1.35);
  graph->GetXaxis()->SetLabelFont(132);
  graph->GetYaxis()->SetLabelFont(132);
  graph->GetYaxis()->SetTitle(yTitle.c_str());
  graph->GetXaxis()->SetTitle("MVA cut");
  //graph->GetYaxis()->SetTitle("");
}

void effmva(double s0 = 104., double es0 = 34, double b0 = 383, double eb0 = 11) {

// max 0.78
 TFile* gfile = new TFile("TMVA.root");
 TH1* eff_S = (TH1*)gfile->Get("Method_MLP/MLPBFGS_L1/MVA_MLPBFGS_L1_effS");
 TH1* eff_B = (TH1*)gfile->Get("Method_MLP/MLPBFGS_L1/MVA_MLPBFGS_L1_effB");

 if (!eff_S || !eff_B) {
   std::cout << "fail to find graphs " << std::endl;
   return;
 } 


 TSpline3* spline_S = new TSpline3(eff_S);
 TSpline3* spline_B = new TSpline3(eff_B);

 double dfdS = (2*b0 + s0)/(2*std::pow(s0 + b0,1.5));
 double dfdB = s0/(2*std::pow(s0 + b0,1.5));
  
 
 double ef0 = sqrt(std::pow(dfdS*es0,2.) + std::pow(dfdB*eb0,2.)) ;
 double f0 = s0/sqrt(s0 + b0);

 double cuts[100];  double values[100];double evalues[100]; double ecuts[100];
 std::cout << "ef0" <<  ef0/f0 << std::endl;
 for (int i =0; i < 100; ++i){

   double cutval = i/100.;
   double s =s0*spline_S->Eval(cutval); 
   double b =b0*spline_B->Eval(cutval);
   double val = s/sqrt(s+b);
   values[i] = val; cuts[i] = cutval;
   evalues[i] = val*ef0/f0;
   ecuts[i] = 0;
   //   std::cout << cutval << " " << val << std::endl;
 }

 TCanvas *c2 = new TCanvas("c2", "c2",10,44,600,400);
 TGraphErrors* graph2 = new TGraphErrors(100, cuts, values, ecuts,evalues);
 plotGraph(graph2, "FOM", 20, 1, 1 );
 graph2->SetMinimum(0);
 graph2->Draw("AL");
 
 // TSpline3* spline = new TSpline3(graph2); 
 TH1F* histo = graph2->GetHistogram();
 histo->Draw("HISTO"); 
 
 Envelope* env = makeEnvelope(graph2);

   env->area.SetLineColor(92);
  env->area.SetFillColor(92);
   env->area.SetFillStyle(1001);
  // env->upper.Draw("SAME");
  // env->lower.Draw("SAME");
   //   env->area.Draw("FSAME");
   env->central.Draw("SAME");
 
 /*TCanvas *c2 = new TCanvas("c2", "c2",10,44,600,400);
 TGraphErrors* graph2 = new TGraphErrors(10,values, beff, evalues, eeff);
 plotGraph(graph2, "Efficiency", 20, 1, 1 );
 graph2->Print();
 graph2->SetMinimum(0);
 graph2->Draw("AL");
 spline_S2->SetLineColor(2);
 spline_S2->Draw("SAME");*/
 //std::cout << spline_S->Eval(0.91) << " " << graph->Eval(0.91) << std::endl;

   std::cout << TMath::MaxElement(graph2->GetN(),graph2->GetY()) << std::endl;
   double max = TMath::MaxElement(graph2->GetN(),graph2->GetY());

   TLine* line = new TLine(0,max, 1, max);
   line->SetLineStyle(2);
   line->SetLineColor(2);
   line->Draw("SAME");

   TLine* line2 = new TLine(0.75,0, 0.75, 9.5);
   line2->SetLineStyle(2);
   line2->SetLineColor(2);
   line2->Draw("SAME");

}
