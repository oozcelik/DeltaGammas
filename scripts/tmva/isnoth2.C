
#include "TGraph.h"

void isnoth2(){

  double cuts[8] = {0, 0.01, 0.02, 0.03, 
                   0.04, 0.05, 0.06, 0.1};
  double fom[8] = {0.096, 0.0964, 0.0975, 0.0978,
                   0.0981, 0.0982, 0.0983, 0.0985 };
  TGraph* graph = new TGraph(8, cuts, fom);
 graph->SetMarkerStyle(20);
// graph->SetMinimum(0);
  graph->Draw("APL");

}
