#ifndef CalScale2015_H
#define CalScale2015_H

#include "TH2D.h"
#include "TAxis.h"
#include "TMath.h"
#include "TObject.h"
#include "TRandom.h"
#include <iostream>
#include <iomanip>

class CalScale2015{

 public:

  //  ClassDef(CalScale2015,1);

  CalScale2015(TH2D* phisto, TH2D* mhisto,  double delta =  -0.075e-3);

  ~CalScale2015(){} 

  double eval(int run, double tx, double ty, int idp) const ;

  double interpolate(int run, double tx, double ty, int idp) const;

  bool inside(const double tx, const double ty, const TH2D* histo) const;

 private:

  int binTx(const double tx, const TH2D* histo) const;
  int binTy(const double ty, const TH2D* histo) const;
  double runOffset(int run) const;

  
  TH2D* m_phisto;  
  TH2D* m_mhisto;  
  double m_delta;

};


inline double CalScale2015::eval(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  int txBin = binTx(tx,histo);
  int tyBin = binTy(ty,histo);
  //  std::cout << histo->GetBinContent(txBin,tyBin)/1000. << " " << runOffset(run)<< std::endl; 
  return 1- ((histo->GetBinContent(txBin,tyBin)/1000.) + runOffset(run) + m_delta);
}

inline double CalScale2015::interpolate(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  double scale;
  if (inside(tx,ty, histo)) {
    scale =  1- ((histo->Interpolate(tx,ty)/1000.) + runOffset(run) + m_delta);
  }
  else {
    // at the edge fall back to bin method
    scale = eval(run,tx,ty,idp);
  }
  return scale;
}


inline CalScale2015::CalScale2015(TH2D* phisto, TH2D* mhisto, double delta): m_phisto(phisto),m_mhisto(mhisto), m_delta(delta) {
  // constructor  
}

inline bool CalScale2015::inside(const double tx, const double ty, const TH2D* histo) const{
   int ix = histo->GetXaxis()->FindBin(tx);
   if (ix == 0 || ix ==  histo->GetNbinsX()+1 ) return false;
   int iy = histo->GetYaxis()->FindBin(ty);
   if (iy == 0 || iy ==  histo->GetNbinsY()+1 ) return false;
   return true;
}

inline int CalScale2015::binTx(const double tx, const TH2D* histo) const {
  int i = histo->GetXaxis()->FindBin(tx);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsX() ) i = histo->GetNbinsX();
  return i;
}

inline int CalScale2015::binTy(const double ty, const TH2D* histo) const {
  int i = histo->GetYaxis()->FindBin(ty);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsY() ) i = histo->GetNbinsY();
  return i;
}

double CalScale2015::runOffset(int run) const {

  double alpha = 0.0;

  

  //down
  if (run >= 162200   && run <= 163200 ) alpha = 0.0;

  //gap
  else if (run >= 163250  && run <= 164000 )  alpha = 0.0406160e-3 ;
  else if (run >= 164001  && run <= 164500 )  alpha = -0.0405840e-3;
  else if (run >= 164501  && run <= 164900 )  alpha = -0.0751180e-3;
  else if (run >= 164901  && run <= 165200 )  alpha =  -0.0706750e-3;

  //gap
  else if (run >= 165201  && run <= 165400 ) alpha =  -0.0708270e-3  ;
  else if (run >= 165401  && run <= 165600 ) alpha =  -0.0670200e-3 ;

  // up
  else if (run >= 166250  && run <= 166400 ) alpha = 0.0940130e-3 ;
  else if (run >= 166401  && run <= 166550 ) alpha = 0.0539590e-3 ;

  // gap
  else if (run >= 166600  && run <= 166700 ) alpha = 0.0157780e-3  ;
  else if (run >= 166700  && run <= 166800 ) alpha = 0.0421130e-3 ;

  // gap
  else if (run >= 166850  && run <= 167000 ) alpha =  -0.0553680e-3 ;
  else if (run >= 167000  && run <= 167150 ) alpha =  -0.0373790e-3;
  
  return alpha;


}


#endif
