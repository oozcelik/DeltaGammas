#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TAxis.h"

#include "TVector3.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include <string>
#include <vector>
#include <utility>
#include "TTree.h"

const double ksMass = 497.67;
const double mKstar = 497.67;
const double mProton = 938.272;
const double mPion = 139.570;
const double mLambda = 1115.7;
const double mmuon = 105.658;

const double mKPlus = 493.677;
const double mPhi = 1019.455; // wrr 0.02
const double mKplus = 493.677;
const double mMuon = 105.658;

const float mJpsi =  3096.870;

inline double dM2(double M, double m1, double m2) {
  return (M*M - m1*m1 - m2*m2);
}

double openingError2(double M, double em , double m1, double m2, double dpp1, double dpp2
) {

  double eo2 = 4*M*M*em*em / TMath::Power(dM2(M, m1, m2), 2);
  return eo2 - dpp1 - dpp2;
}

double eMOA(double eo, double M, double m1, double m2 ){

  return (0.5*eo * dM2(M, m1, m2)/M);

}


double newDM(double M, double m1, double m2, double dpp1, double dpp2, double eo2){

  double relError = eo2 + dpp1 + dpp2 ;
  double factor = 0.5*dM2(M, m1, m2)/M;
  return factor*sqrt(relError);
}

bool inIT(double x, double y){

  if (y > - 11 && y < 11 && x < -9.9  && x > -62.7 ) return true;
  if (y > - 11 && y < 11 && x > 9.9  && x < 62.7 ) return true;
  if (y < - 9.9 && y > -20.7 && x < 31.5  && x > -31.5 ) return true;
  if (y >   9.9 && y < 20.7 && x < 31.5  && x > -31.5 ) return true;

  return false;
}

float phi(float x, float y) {
  return atan(y/x);
}

float r(float x, float y) {
  return sqrt(x*x + y*y);
}

float theta(float r , float z) {
  return atan(r/z);
}

float thetaToRapidity(float angle){
  return -log(tan(angle/2.));
}

void xyToEtaPhi(float x,float y,float z, float& teta, float& tphi ){
  tphi = phi(x,y);
  float tR = r(x,y);
  teta =thetaToRapidity(theta(tR,z));
}

bool inOpenVelo(float eta, float phi){

  if (eta > 0.85 && eta < 3.55) {
    if (phi > 2.1 && phi < 4.1) return true;
    if (phi > 0 && phi < 1) return true;
    if (phi > 5.3 && phi <  2*TMath::Pi() ) return true;
  }
  return false;
}

bool inHalfOpenVelo(float eta, float phi){

  if (eta > 1 && eta < 4) {
    if (phi > 1.8 && phi < 4.4) return true;
    if (phi > 0 && phi < 1.2) return true;
    if (phi > 5. && phi <  2*TMath::Pi() ) return true;
  }
  return false;
}

int pbin(float p){

  // binning for histograms in p

  int bin = 0;
  if ( p < 10) {
    bin =0;
  }
  else if (p > 10 && p < 15) {
    bin = 1;
  }
  else if (p > 15 && p < 20){
    bin = 2;
  }
  else  if (p > 20 && p < 30 ) {
    bin = 3;
  }
  else if ( p > 30){
    bin = 4;
  }

  return bin;
}


class ksCand{

 public:

 ksCand(float FD_PV = 20, float pMin = 2000, float ip = 3, float dira = 0.9999, float tauchi2 = 0., float zMin = -800, float zMax = 2200.):
    m_FD_PV(FD_PV),
    m_pMin(pMin),
    m_ip(ip),
    m_dira(dira),
    m_tauchi2(tauchi2),
    m_zMin(zMin),
    m_zMax(zMax)
{}

  ~ksCand(){}

  // select good ks candidates
  bool isGood(float FD_PV, float pMin, float ip, float dira, float tauchi2, float zVert){
    if (FD_PV < m_FD_PV) return false;
    if (pMin < m_pMin) return false;
    if (ip < m_ip) return false;
    if (dira < m_dira) return false;
    if (tauchi2 < m_tauchi2) return false;
    if (zVert < m_zMin || zVert > m_zMax) return false;
    return true;
  }

 private:

  float m_FD_PV;
  float m_pMin;
  float m_ip;
  float m_dira;
  float m_tauchi2;
  float m_zMin;
  float m_zMax;

};

enum KStypes{
  kLong,
  kDown,
  kMixed
};

KStypes type(int t1, int t2){

  // type

  if (t1 == t2 && t1 == 3) return kLong;
  if (t1 == t2 && t1 == 5) return kDown;
  return kMixed;
}

std::string typeToString(const KStypes ktype){

  std::string theType = "kMixed";
  if (ktype == kLong) return "kLong";
  if (ktype == kDown) return "kDown";
  return theType;
}

class OutputNameCreator{

 public:

  ~OutputNameCreator(){}

  OutputNameCreator(std::string header, std::string footer):
    m_header(header), m_footer(footer) {}

  std::string name(const char* location ) {
    std::string stringName = std::string(location);
    return name(stringName); }

  std::string name(std::string location ) {return m_header + location + m_footer ; }


 private:

    std::string m_header;
    std::string m_footer;

};

int typeInT(double x1, double y1, double x2, double y2){

  int theType = 0;
  if (inIT(x1/10, y1/10) == true) ++theType;
  if (inIT(x2/10, y2/10) == true) ++theType;
  return theType;
}



void plotHisto(TCanvas* tCan, int pad,  TH1F* aHisto, char* ax, double rangeMin, double rangeMax, std::string output = ""){

  tCan->cd(pad);

  aHisto->SetTitle("");
  aHisto->Draw("HISTO");
  TAxis* xachse = aHisto->GetXaxis();
  xachse->SetRangeUser(rangeMin,rangeMax);
  xachse->SetTitleFont (132);
  xachse->SetLabelFont (132);
  xachse->SetTitle(ax);
  aHisto->GetYaxis()->SetLabelFont(132);
  aHisto->GetYaxis()->SetTitleFont(132);
  aHisto->GetYaxis()->SetTitle("# events");
  aHisto->GetYaxis()->SetTitleOffset(1.2);
  aHisto->Draw("HISTO");
  if (output != "") tCan->Print(output.c_str());
}


void plotHisto2D(TCanvas* tCan, int pad,  TH2F* aHisto, char* ax, char* ay, std::string output = ""){

  tCan->cd(pad);

  aHisto->SetTitle("");
  TAxis* xachse = aHisto->GetXaxis();
  xachse->SetTitleFont (132);
  xachse->SetLabelFont (132);
  xachse->SetTitle(ax);
  aHisto->GetYaxis()->SetLabelFont(132);
  aHisto->GetYaxis()->SetTitleFont(132);
  aHisto->GetYaxis()->SetTitle(ay);
  aHisto->Draw("col+cont2");
  if (output != "") tCan->Print(output.c_str());
}

class MassCut{

 public:

  MassCut(double min, double max): m_min(min), m_max(max)  {}

  MassCut(double nsigma, double  resolution, double mean ): m_min(mean - nsigma*resolution), m_max(mean + nsigma*resolution) {}

  ~MassCut(){}

  bool inWindow(double m) { return m > m_min && m < m_max ; }

  double min() const {return m_min;}

  double max() const {return m_max;}

 private:

  double m_min;
  double m_max;

};

#include <iostream>
bool marine(float before1, float after1, float before2, float after2){
  bool tmarine = false;
  if (before1 > 0 && before2 < 0){
    if (after1 > before1) tmarine = true;
  }
  else if (before1 < 0 && before2 > 0){
    if (after2 > before2 )tmarine = true;
  }
  else if (before1 > 0 && before2 > 0) {
    if (before1 > before2){
      if (after1 > before1) tmarine = true;
    }
    else {
      if (after2 > before2) tmarine = true;
    }
  }
  else if (before1 < 0 && before2 < 0){
    if (before1 < before2 ){
      if (after1 < before1) tmarine = true;
    }
    else {
      if (after2 < before2) tmarine = true;
    }
  }
  else {
    std::cout << "case not considered" << std::endl;
  }


  return tmarine;
}

float R(float p1, float p2) {

  float ratio = p1/p2;
  return 2 + ratio + 1./ratio;
}

float chi2dof(float chi2, int dof){
  return dof > 0 ? chi2/float(dof): -999.;
}

bool isLambda(float piminus_PX, float piminus_PY, float piminus_PZ,
              float piplus_PX,  float piplus_PY, float piplus_PZ){

  MassCut mCut(3, 4 , mLambda);
  TVector3 plusV = TVector3(piplus_PX,piplus_PY,piplus_PZ);
  TVector3 minusV = TVector3(piminus_PX,piminus_PY,piminus_PZ);
  TLorentzVector fProtonPlus = TLorentzVector(plusV, TMath::Sqrt(mProton*mProton + plusV.Mag2()));
  TLorentzVector fPionMinus =  TLorentzVector( minusV, TMath::Sqrt(mPion*mPion + minusV.Mag2()) );
  TLorentzVector lambdaHyp = fProtonPlus + fPionMinus;
  if (mCut.inWindow(lambdaHyp.M())) return true;

  TLorentzVector fPionPlus = TLorentzVector(plusV, TMath::Sqrt(mPion*mPion + plusV.Mag2()));
  TLorentzVector fProtonMinus = TLorentzVector( minusV, TMath::Sqrt(mProton*mProton + minusV.Mag2()) );
  TLorentzVector lambdabarHyp = fProtonMinus + fPionPlus;
  if (mCut.inWindow(lambdabarHyp.M())) return true;
  return false;
}

float min_element(float* values, int n ){

  unsigned int minvalue = values[0];
  for (int i = 0; i < n; ++i){
    if (values[i] < minvalue) minvalue = values[i];
    std::cout << values[i] << std::endl;
  }
  return minvalue;
}

float max_element(float* values, int n ){

  unsigned int maxvalue = values[0];
  for (int i = 0; i < n; ++i){
    if (values[i] > maxvalue) maxvalue = values[i];
  }
  return maxvalue;
}


float px(float tx, float ty, float p){
 float norm = sqrt(1 + tx*tx + ty*ty);
 return tx*p/norm;
}



TLorentzVector makeFourVector(float tx, float ty, float p, float m) {

 const double rnorm = 1/sqrt(1 + tx*tx + ty*ty);
 const double px =  tx*p*rnorm;
 const double py =  ty*p*rnorm;
 const double pz =  p*rnorm;
 const TVector3 vec = TVector3(px, py, pz);
 //TLorentzVector fourVec = TLorentzVector(vec, TMath::Sqrt(m*m + vec.Mag2()));
 //return fourVec;
 return TLorentzVector(vec, TMath::Sqrt(m*m + vec.Mag2()));
}


TLorentzVector toFourVector(const float& px, const float& py, const float& pz, const float& m) noexcept {

 const TVector3 vec = TVector3(px, py, pz);
 return TLorentzVector(vec, TMath::Sqrt(m*m + vec.Mag2()));
}



float updateMass(float tx1, float ty1, float p1, float m1, float tx2, float ty2, float p2, float m2){

  const TLorentzVector vec1 = makeFourVector(tx1,ty1,p1, m1);
  const TLorentzVector vec2 = makeFourVector(tx2,ty2,p2, m2);
  const TLorentzVector sumVec = vec1 + vec2;
  return sumVec.Mag();
}


float scaleFactor(float R) {

  double top = (R*mPion*mPion) - (ksMass*ksMass);
  return top/ksMass;
}

class SelectedEvents {

 public:

  SelectedEvents(){;}

  ~SelectedEvents(){;}

  bool inList(unsigned int event, unsigned int run){

    bool good = true;
    for (unsigned int i = 0; i < m_list.size() ; ++i){
      if (m_list[i].first == event && m_list[i].second== run){
	// we have this before
        good = false;
      }
      else {
	m_list.push_back(std::make_pair(event,run));
      }
    }
    return good;
  }

 private:

  std::vector< std::pair<unsigned int, unsigned int > > m_list;

};


enum Quad{

  Q1 =1, Q2, Q3, Q4

};

Quad toQuadrant(float x, float y){

  if (x > 0.0 ){
    return y > 0.0 ?  Q1 : Q3;
  }
  else {
    return y > 0.0 ?  Q2 : Q4;
  }
}

enum Topo{
  Q11 = 1, Q22, Q33, Q44, Q12, Q34, Q14, Q23, Q13, Q24
};

enum Topo toTopo(Quad q1, Quad q2 ){

  int i1 = q1; int i2 = q2;
  if (i1 == i2) {
    return Topo(i1);
  }

  if (i1 > i2){
    std::swap(q1,q2);
  }

  if ( i1 == 1){
    if (i2 == 2) return Q12;
    if (i2 == 3) return Q13;
    if (i2 == 4) return Q14;
  }

  if (i1 == 2) {
    if (i2 == 3) return Q23;
    if (i2 == 4) return Q24;
  }

  return Q34;
}

bool isUpper(Topo t1){

  bool up = false;
  if (t1 == Q11 || t1 == Q22 || t1 == Q12  ) up = true;
  return up;
}


float dAngle(float px1, float py1, float pz1, float px2, float py2, float pz2  ){

 TVector3 u1 = TVector3(px1,py1,pz1);
 TVector3 u2 = TVector3(px2,py2,pz2);
 TVector3 nu1 = u1.Unit();  TVector3 nu2 = u2.Unit();
 TVector3 cr = nu1.Cross(nu2);
 TVector3 norm = cr.Unit();
 TVector3 bField(0.,1.,0.);
 double angle = norm.Dot(bField);
 return TMath::ACos(angle);
}



float oAngle(float px1, float py1, float pz1, float px2, float py2, float pz2  ){

 TVector3 u1 = TVector3(px1,py1,pz1);
 TVector3 u2 = TVector3(px2,py2,pz2);
 TVector3 nu1 = u1.Unit();  TVector3 nu2 = u2.Unit();
 double angle = nu1.Dot(nu2);
 return TMath::ACos(angle);
}

float oAngle(float tx1, float ty1, float tx2, float ty2 ){


 TVector3 u1 = TVector3(tx1,ty1,1);
 TVector3 u2 = TVector3(tx2,ty2,1);
 TVector3 nu1 = u1.Unit();  TVector3 nu2 = u2.Unit();
 double angle = nu1.Dot(nu2);
 return TMath::ACos(angle);
}

void fillVector(std::vector<int>& tValues, TTree* tree){
 int run;
 tree->SetBranchAddress("run",&run);
 tValues.reserve(20000);
 int nevent = tree->GetEntries();
 //nevent = 100000;
 int i = 0;
  tValues.push_back(0);
  while (i  < nevent){
    tree->GetEntry(i);
    tValues.push_back(run);
    ++i;
  }
  std::sort(tValues.begin(), tValues.end());
  tValues.erase(std::unique(tValues.begin(), tValues.end()), tValues.end());
}

float toPT(double px, double py){
  return sqrt(px*px + py*py);
}

float toP(double px, double py, double pz){
  return(sqrt(px*px + py*py + pz*pz ));
}

TVector3 directionVector(double xPrim, double yPrim, double zPrim,
                         double xv, double yv, double zv){

  TVector3 vec(xv - xPrim, yv - yPrim , zv - zPrim);
  TVector3 uvec =  vec.Unit();
  return uvec;
}

double ptMiss(TVector3& dira, TVector3& p){
  const double pMag2 = p.Mag2();
  const double pL = p.Dot(dira);
  return sqrt(pMag2 - pL*pL );
}

double mMiss(double m, double ptmiss){
  return sqrt(m*m + ptmiss*ptmiss) + ptmiss;
}

/*bool inIT(double x, double y){

  if (y > - 11 && y < 11 && x < -9.9  && x > -62.7 ) return true;
  if (y > - 11 && y < 11 && x > 9.9  && x < 62.7 ) return true;
  if (y < - 9.9 && y > -20.7 && x < 31.5  && x > -31.5 ) return true;
  if (y >   9.9 && y < 20.7 && x < 31.5  && x > -31.5 ) return true;

  return false;
}
*/
/*
int runperiod (int run){
  int period = -1;
  if (run >= 87219  && run <=  89777 ) period = 0;
  else if (run >= 89333  && run <=  90256 ) period =1 ;
  else if (run >= 90257  && run <=  93282 ) period = 2;
  else if (run >= 93398  && run <=  94386 ) period = 3;
  else if (run >= 95948  && run <=  97028 ) period = 4;
  else if (run >= 97114  && run <=  98882 ) period = 5;
  else if (run >= 98900  && run <=  101862 ) period =6;
  else if (run >= 101891  && run <=  102452 ) period =7;
  else if (run >= 102499  && run <=  102907 ) period =8;

  else if (run >= 103049  && run <=  103687 ) period = 9;
  else if (run >= 103954  && run <=  104414 ) period = 10;
  return period;
}

*/

int runperiod (int run){
  int period = -1;
  if (run >= 87219  && run <=  89777 ) period = 0;
  else if (run >= 89333  && run <=  90256 ) period =1 ;
  else if (run >= 90257  && run <=  90763 ) period = 2;
  else if (run >= 91556 && run <= 93282) period = 3;
  else if (run >= 93398  && run <=  94386 ) period = 4;
  else if (run >= 95948  && run <=  97028 ) period = 5;
  else if (run >= 97114  && run <=  98882 ) period = 6;
  else if (run >= 98900  && run <=  101862 ) period =7;
  else if (run >= 101891  && run <=  102452 ) period =8;
  else if (run >= 102499  && run <=  102907 ) period =9;

  else if (run >= 103049  && run <=  103687 ) period = 10;
  else if (run >= 103954  && run <=  104414 ) period = 11;
  return period;
}


int runperiod12 (int run){
  int period = -1;
  if (run >= 111183   && run <= 114205 ) period = 0;
  else if (run >= 114205  && run <= 114287 )  period =1 ;
  else if (run >= 114316  && run <= 115464  ) period = 2;
  else if (run >= 115518 && run <=117103 ) period = 3;
  else if (run >= 117192 && run <= 118286   ) period = 4;
  else if (run >= 118326   && run <=123803    ) period = 5;
  else if (run >=123910   && run <= 125115  ) period = 6;
  else if (run >= 125566  && run <= 126680  ) period =7;
  else if (run >=126824  && run <=  128110 ) period =8;
  else if (run >=128411   && run <= 129978 ) period =9;
  else if (run >= 130316  && run <= 130861) period = 10;
  else if (run >= 130911    && run <= 131940  ) period = 11;
  else if (run >=  131973 && run <= 133588   ) period = 12;
  else if (run >= 133624  && run <= 134000  ) period = 13;
  return period;
}


double runOffset(int run) {
  double alpha = 0.0;
  if (run >= 87219  && run <=  89777 ) alpha =0.0;
  else if (run >= 89333  && run <=  90256 ) alpha = 9.97629e-05 ;
  else if (run >= 90257  && run <=   90763  ) alpha = 6.01683e-06;
  else if (run >= 91556 && run <= 93282)  alpha = 5.33486e-05 ;
  else if (run >= 93398  && run <=  94386 ) alpha = 0.000183575;
  else if (run >= 95948  && run <=  97028 ) alpha =0.000317198 ;
  else if (run >= 97114  && run <=  98882 ) alpha = 0.000135501;
  else if (run >= 98900  && run <=  101862 ) alpha =0.000182945 ;
  else if (run >= 101891  && run <=  102452 ) alpha = 8.26117e-05;
  else if (run >= 102499  && run <=  102907 ) alpha = 0.000209637;

  else if (run >= 103049  && run <=  103687 ) alpha = 8.89579e-05;
  else if (run >= 103954  && run <=  104414 ) alpha = 0.000288616;
  return alpha;
}

double runOffset12(int run) {
  double alpha = 0.0;
  if (run >= 111183   && run <= 114205 ) alpha = 0.0 ;
  else if (run >= 114205  && run <= 114287 ) alpha =0.000107279  ;
  else if (run >= 114316  && run <= 115464  ) alpha =-2.0368e-05  ;
  else if (run >= 115518 && run <=117103 ) alpha = 7.00774e-05 ;
  else if (run >= 117192 && run <= 118286   )alpha =3.8054e-05  ;
  else if (run >= 118326   && run <=123803    )alpha =0.000185025 ;
  else if (run >=123910   && run <= 125115  ) alpha =8.58198e-05  ;
  else if (run >= 125566  && run <= 126680  ) alpha = 0.000240643  ;
  else if (run >=126824  && run <=  128110 ) alpha = 0.000188533 ;
  else if (run >=128411   && run <= 129978 ) alpha = 0.000207181;
  else if (run >= 130316  && run <= 130861) alpha = 3.76936e-05  ;
  else if (run >= 130911    && run <= 131940  ) alpha = 0.000240579  ;
  else if (run >=  131973 && run <= 133588   ) alpha = -0.000826221 ;
  else if (run >= 133624  && run <= 134000  )alpha =  -0.000826221 ;

  return alpha;
}

/*
double runPeriod15(int run){

  int period = -1;
  if (run >= 162200   && run <= 163200 ) period = 0;
  else if (run >= 163250  && run <= 165200 )  period =1 ;
  else if (run >= 165201  && run <= 165600 )  period =2 ;
  else if (run >= 166250  && run <= 166550 )  period =3 ;
  else if (run >= 166600  && run <= 166800 )  period =4 ;
  else if (run >= 166850  && run <= 167150 )  period =5 ;

  return period;

}
*/

int runPeriod15(int run){

  int period = -1;
  //down
  if (run >= 162200   && run <= 163200 ) period = 0;

  //gap
  else if (run >= 163250  && run <= 164000 )  period =1 ;
  else if (run >= 164001  && run <= 164500 )  period =2 ;
  else if (run >= 164501  && run <= 164900 )  period =3 ;
  else if (run >= 164901  && run <= 165200 )  period =4 ;

  //gap
  else if (run >= 165201  && run <= 165400 )  period =5 ;
  else if (run >= 165401  && run <= 165600 )  period =6 ;

  // up
  else if (run >= 166250  && run <= 166400 )  period =7 ;
  else if (run >= 166401  && run <= 166550 )  period =8 ;

  // gap
  else if (run >= 166600  && run <= 166700 )  period =9 ;
  else if (run >= 166700  && run <= 166800 )  period =10 ;

  // gap
  else if (run >= 166850  && run <= 167000 )  period =11 ;
  else if (run >= 167000  && run <= 167150 )  period =12 ;

  return period;

}

int runPeriod16(int run){

  int period = -1;
  //down
  if (run >=174500   && run <= 175100 ) period = 0;
  else if (run >= 175101  && run <= 175900 )  period =1 ;
  else if (run >= 175901  && run <= 176100 )  period =2 ;
  else if (run >= 176101  && run <= 176330 )  period =3 ;

  //up
  else if (run >= 176657 && run <= 177000) period = 4;
  else if (run >= 177001 && run <= 177250) period = 5;
  else if (run >= 177251 && run <= 177350) period = 6;
  else if (run >= 177351 && run <= 177500) period = 7;

  else if (run >= 177501 && run <= 177600) period = 8;
  else if (run >= 177601 && run <= 177800) period = 9;

  else if (run >= 178001 && run <= 178250) period = 10;
  else if (run >= 178251 && run <= 178500) period = 11;
  else if (run >= 178501 && run <= 179001) period = 12;
  else if (run >= 179001 && run <= 179500) period = 13;
  else if (run >= 179501 && run <= 179999) period = 14;
  else if (run >= 180000 && run <= 180300) period = 15;
  else if (run >= 180301 && run <= 180640) period = 16;

  // down
  else if (run >= 181200  && run <= 181700 )  period =17 ;
  else if (run >= 181700  && run <= 182300 )  period =18;
  else if (run >= 182301  && run <= 182500 )  period =19 ;
  else if (run >= 182501  && run <= 182800 )  period =20 ;
  else if (run >= 182801  && run <= 183000 )  period =21 ;
  else if (run >= 183001  && run <= 183200 )  period =22 ;

  //up
  else if (run >= 183800  && run <= 184200 )  period =23 ;
  else if (run >= 184201  && run <= 184657 )  period =24 ;


  // down
  else if (run >= 184700  && run <= 185000 )  period =25 ;
  else if (run >= 185000  && run <= 185100 )  period =26 ;
  else if (run >= 185101  && run <= 185200 )  period =27 ;
  else if (run >= 185201  && run <= 185300 )  period =28 ;
  else if (run >= 185301  && run <= 185400 )  period =29 ;
  else if (run >= 185401  && run <= 185600 )  period =30 ;



  return period;

}


double runOffset15(int run) {
  double alpha = 0.0;

  //down
  if (run >= 162200   && run <= 163200 ) alpha = 0.0;

  //gap
  else if (run >= 163250  && run <= 164000 )  alpha = 0.0406160e-3 ;
  else if (run >= 164001  && run <= 164500 )  alpha = -0.0405840e-3;
  else if (run >= 164501  && run <= 164900 )  alpha = -0.0751180e-3;
  else if (run >= 164901  && run <= 165200 )  alpha =  -0.0706750e-3;

  //gap
  else if (run >= 165201  && run <= 165400 ) alpha =  -0.0708270e-3  ;
  else if (run >= 165401  && run <= 165600 ) alpha =  -0.0670200e-3 ;

  // up
  else if (run >= 166250  && run <= 166400 ) alpha = 0.0940130e-3 ;
  else if (run >= 166401  && run <= 166550 ) alpha = 0.0539590e-3 ;

  // gap
  else if (run >= 166600  && run <= 166700 ) alpha = 0.0157780e-3  ;
  else if (run >= 166700  && run <= 166800 ) alpha = 0.0421130e-3 ;

  // gap
  else if (run >= 166850  && run <= 167000 ) alpha =  -0.0553680e-3 ;
  else if (run >= 167000  && run <= 167150 ) alpha =  -0.0373790e-3;

  return alpha;


}

double runOffset16(int run){


  //  std::cout << run << std::endl;
  double alpha = 0;
  //down
  if (run >=174500   && run <= 175100 ) alpha = 0;
  else if (run >= 175101  && run <= 175900 )  alpha =0.0302171e-3 ;
  else if (run >= 175901  && run <= 176100 )  alpha =0.0558559e-3 ;
  else if (run >= 176101  && run <= 176330 )  alpha = 0.0874364e-3;

  //up
  else if (run >= 176657 && run <= 177000) alpha = 0.130490e-3;
  else if (run >= 177001 && run <= 177250) alpha = 0.146611e-3;
  else if (run >= 177251 && run <= 177350) alpha =  0.166163e-3;
  else if (run >= 177351 && run <= 177500) alpha =  0.153974e-3;

  else if (run >= 177501 && run <= 177600) alpha =  0.239742e-3;
  else if (run >= 177601 && run <= 177800) alpha = 0.299172e-3;

  else if (run >= 178001 && run <= 178250) alpha = 0.238894e-3;
  else if (run >= 178251 && run <= 178500) alpha = 0.221683e-3;
  else if (run >= 178501 && run <= 179001) alpha = 0.232447e-3;
  else if (run >= 179001 && run <= 179500) alpha = 0.251556e-3;
  else if (run >= 179501 && run <= 179999) alpha = 0.246533e-3;
  else if (run >= 180000 && run <= 180300) alpha = 0.270993e-3;
  else if (run >= 180301 && run <= 180640) alpha = 0.255366e-3;

  // down
  else if (run >= 181200  && run <= 181700 )  alpha = 0.0686362e-3 ;
  else if (run >= 181700  && run <= 182300 )  alpha =0.0945660e-3;
  else if (run >= 182301  && run <= 182500 )  alpha =0.0683000e-3 ;
  else if (run >= 182501  && run <= 182800 )  alpha = 0.0877048e-3;
  else if (run >= 182801  && run <= 183000 )  alpha = 0.126342e-3;
  else if (run >= 183001  && run <= 183200 )  alpha = 0.119248e-3;

  //up
  else if (run >= 183800  && run <= 184200 )  alpha =0.222877e-3 ;
  else if (run >= 184201  && run <= 184657 )  alpha = 0.211130e-3;


  // down
  else if (run >= 184700  && run <= 185000 )  alpha =0.2111e-3 ;
  else if (run >= 185000  && run <= 185100 )  alpha =0.05129e-3 ;
  else if (run >= 185101  && run <= 185200 )  alpha = -0.00853040e-3 ;
  else if (run >= 185201  && run <= 185300 )  alpha =0.0154273e-3 ;
  else if (run >= 185301  && run <= 185400 )  alpha =0.0351537e-3 ;
  else if (run >= 185401  && run <= 185600 )  alpha = 0.0677423e-3 ;

  //  std::cout << run << " " << alpha << std::endl;

  return alpha;

}



bool requiredPolarity(int pol, float polarity){

  //  std::cout << pol << std::endl;
  bool take = false;
  if (pol == 0) {

    take = true;
  }
  else if (pol == -1 && polarity < 0){
    take = true;
  }
  else if (pol == 1 && polarity > 0){
    take = true;
  }

  return take;

}

bool requiredC(int c, int id){
  int take = false;
  if (c == 0) {
    take = true;
  }
  else if (c == id) {
    take = true;
  }
  return take;
}


bool requiredIDP(int c, int id){
  int take = false;
  if (c == 0) {
    take = true;
  }
  else if (c == id) {
    take = true;
  }
  return take;
}



bool requiredAlignment(int i, int ia){
  int take = false;
  if (i == 0) {
    take = true;
  }
  else if (i == ia) {
    take = true;
  }
  return take;
}

Float_t coshel(TLorentzVector particle, TLorentzVector parent, TLorentzVector grandparent) {
    TVector3 boosttoparent = -(parent.BoostVector());

    particle.Boost(boosttoparent);
    grandparent.Boost(boosttoparent);

    TVector3 particle3 = particle.Vect();
    TVector3 grandparent3 = grandparent.Vect();

    Float_t numerator = particle3.Dot(grandparent3);
    Float_t denominator = (particle3.Mag())*(grandparent3.Mag());
    Float_t temp = numerator/denominator;

    return temp;
}
