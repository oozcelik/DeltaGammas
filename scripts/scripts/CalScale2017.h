#ifndef CalScale2017_H
#define CalScale2017_H

#include "TH2D.h"
#include "TAxis.h"
#include "TMath.h"
#include "TObject.h"
#include "TRandom.h"
#include <iostream>
#include <iomanip>

class CalScale2017{

 public:

  //  ClassDef(CalScale2015,1);

 
  CalScale2017(TH2D* phisto, TH2D* mhisto,  double delta = -0.155e-3);

  ~CalScale2017(){} 

  double eval(int run, double tx, double ty, int idp) const ;

  double interpolate(int run, double tx, double ty, int idp) const;

  bool inside(const double tx, const double ty, const TH2D* histo) const;

 private:

  int binTx(const double tx, const TH2D* histo) const;
  int binTy(const double ty, const TH2D* histo) const;
  double runOffset(int run) const;

  
  TH2D* m_phisto;  
  TH2D* m_mhisto;  
  double m_delta;

};


inline double CalScale2017::eval(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  int txBin = binTx(tx,histo);
  int tyBin = binTy(ty,histo);
  //  std::cout << histo->GetBinContent(txBin,tyBin)/1000. << " " << runOffset(run)<< std::endl; 
  return 1- ((histo->GetBinContent(txBin,tyBin)/1000.) + runOffset(run) + m_delta);
}

inline double CalScale2017::interpolate(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  double scale;
  if (inside(tx,ty, histo)) {
    scale =  1- ((histo->Interpolate(tx,ty)/1000.) + runOffset(run) + m_delta);
  }
  else {
    // at the edge fall back to bin method
    scale = eval(run,tx,ty,idp);
  }
  return scale;
}


inline CalScale2017::CalScale2017(TH2D* phisto, TH2D* mhisto, double delta): m_phisto(phisto),m_mhisto(mhisto), m_delta(delta) {
  // constructor  
}

inline bool CalScale2017::inside(const double tx, const double ty, const TH2D* histo) const{
   int ix = histo->GetXaxis()->FindBin(tx);
   if (ix == 0 || ix ==  histo->GetNbinsX()+1 ) return false;
   int iy = histo->GetYaxis()->FindBin(ty);
   if (iy == 0 || iy ==  histo->GetNbinsY()+1 ) return false;
   return true;
}

inline int CalScale2017::binTx(const double tx, const TH2D* histo) const {
  int i = histo->GetXaxis()->FindBin(tx);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsX() ) i = histo->GetNbinsX();
  return i;
}

inline int CalScale2017::binTy(const double ty, const TH2D* histo) const {
  int i = histo->GetYaxis()->FindBin(ty);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsY() ) i = histo->GetNbinsY();
  return i;
}

double CalScale2017::runOffset(int run) const {

  double alpha = 0.15e-3; // reasonable value for the 5 TeV run

  if (run >= 190000   && run <=  193500 ) alpha =0.198731e-3 ;
  else if (run >= 193500   && run <=  193800 ) alpha = 0.266449e-3 ;
  else if (run >= 193801   && run <=  194000 ) alpha = 0.238547e-3 ;
  else if (run >= 194000   && run <=  194400 ) alpha = 0.269386e-3 ;

  // down
  else if (run >= 194700   && run <=  195700 ) alpha = 0.0585973e-3;
  else if (run >= 195900   && run <=  196000 ) alpha = 0.105339e-3;
  else if (run >= 196001   && run <=  196500 ) alpha = 0.111267e-3;
  else if (run >= 196501   && run <=  196750 ) alpha = 0.075941e-3;
  else if (run >= 196751   && run <=  196850 ) alpha = 0.108717e-3;
  else if (run >= 196851   && run <=  197000 ) alpha = 0.0885255e-3;

  // up
  else if (run >= 197001   && run <=  197500 ) alpha = 0.211224e-3;
  else if (run >= 197501   && run <=  197800 ) alpha = 0.208363e-3;
  else if (run >= 197800   && run <=  198500 ) alpha = 0.262614e-3;
  else if (run >= 198501   && run <=  199200 ) alpha = 0.271458e-3;

  // down
  else if (run >= 199400   && run <=  200400 ) alpha = 0.0991116e-3;
  else if (run >= 200401   && run <= 201200  ) alpha = 0.0972021e-3;

  // up
  else if (run >=  201201  && run <=  201600 ) alpha = 0.261225e-3;
  else if (run >=  201601  && run <=  202100 ) alpha = 0.252726e-3;
  else if (run >=  202600 && run <=  203000 ) alpha = 0.185992e-3;

  return alpha;
  



}


#endif
