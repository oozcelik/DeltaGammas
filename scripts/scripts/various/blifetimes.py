#!/usr/bin/env python

#dddbtag = "dddb-20150724" # Pi Pi
#dddbtag = "Sim08-20130503-1" # Etaprime
dddbtag = "dddb-20170721-3" # Etaprime

from Configurables import LHCbApp
LHCbApp().DDDBtag = dddbtag

# =============================================================================
import PartProp.PartPropAlg
import PartProp.Service
from   GaudiPython.Bindings import AppMgr
# =============================================================================

gaudi = AppMgr()

gaudi.initialize()

pps   = gaudi.ppSvc()

buprop = pps.find('B+')
bdprop = pps.find('B0')
bsprop = pps.find('B_s0')

print dddbtag
print '\nB+ lifetime        = %.3f ps\n' % (buprop.lifeTime() * 1e3)
print '\nB0 lifetime        = %.3f ps\n' % (bdprop.lifeTime() * 1e3)
print '\nBs lifetime        = %.3f ps\n' % (bsprop.lifeTime() * 1e3)
