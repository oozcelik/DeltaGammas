#ifndef CalScale_H
#define CalScale_H

#include "TH2D.h"
#include "TAxis.h"
#include "TMath.h"
#include "TObject.h"
#include "TRandom.h"
#include <iostream>
#include <iomanip>

class CalScale: public TObject{

 public:

  // ClassDef(CalScale,1);

  CalScale(TH2D* phisto, TH2D* mhisto,  double delta = 1.0e-4 );

  ~CalScale(){} 

  double eval(int run, double tx, double ty, int idp) const ;

  double interpolate(int run, double tx, double ty, int idp) const;

  bool inside(const double tx, const double ty, const TH2D* histo) const;

 private:

  int binTx(const double tx, const TH2D* histo) const;
  int binTy(const double ty, const TH2D* histo) const;
  double runOffset(int run) const;

  TH2D* m_phisto;  
  TH2D* m_mhisto;  
double m_delta;
};


inline double CalScale::eval(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  int txBin = binTx(tx,histo);
  int tyBin = binTy(ty,histo);
  //  std::cout << histo->GetBinContent(txBin,tyBin)/1000. << " " << runOffset(run)<< std::endl; 
  return 1- ((histo->GetBinContent(txBin,tyBin)/1000.) + runOffset(run) + m_delta);
}

inline double CalScale::interpolate(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  double scale;
  if (inside(tx,ty, histo)) {
    scale =  1- ((histo->Interpolate(tx,ty)/1000.) + runOffset(run) + m_delta);
  }
  else {
    // at the edge fall back to bin method
    scale = eval(run,tx,ty,idp);
  }
  return scale;
}


inline CalScale::CalScale(TH2D* phisto, TH2D* mhisto, double delta): m_phisto(phisto),m_mhisto(mhisto), m_delta(delta) {
  // constructor  
}

inline bool CalScale::inside(const double tx, const double ty, const TH2D* histo) const{
   int ix = histo->GetXaxis()->FindBin(tx);
   if (ix == 0 || ix ==  histo->GetNbinsX()+1 ) return false;
   int iy = histo->GetYaxis()->FindBin(ty);
   if (iy == 0 || iy ==  histo->GetNbinsY()+1 ) return false;
   return true;
}

inline int CalScale::binTx(const double tx, const TH2D* histo) const {
  int i = histo->GetXaxis()->FindBin(tx);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsX() ) i = histo->GetNbinsX();
  return i;
}

inline int CalScale::binTy(const double ty, const TH2D* histo) const {
  int i = histo->GetYaxis()->FindBin(ty);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsY() ) i = histo->GetNbinsY();
  return i;
}

inline double CalScale::runOffset(int run) const {
  double alpha = 0.0;
  if (run >= 87219  && run <=  89777 ) alpha =0.0; 
  else if (run >= 89333  && run <=  90256 ) alpha = 6.36699e-05  ;
  else if (run >= 90257  && run <=   90763  ) alpha = 7.11719e-05 ;
  else if (run >= 91556 && run <= 93282)  alpha = 1.53674e-05 ;
  else if (run >= 93398  && run <=  94386 ) alpha = 0.000144135 ; 
  else if (run >= 95948  && run <=  97028 ) alpha = 0.000214408  ;
  else if (run >= 97114  && run <=  98882 ) alpha = 3.41493e-05 ;
  else if (run >= 98900  && run <=  101862 ) alpha =  0.000137622;
  else if (run >= 101891  && run <=  102452 ) alpha = 3.73981e-05 ;
  else if (run >= 102499  && run <=  102907 ) alpha = 0.000169023 ;

  else if (run >= 103049  && run <=  103687 ) alpha =  3.20303e-05 ;
  else if (run >= 103954  && run <=  104414 ) alpha =  0.00017584 ;

 
  return alpha;
}


#endif
