#include <vector>
#include <string>
#include <iostream>

void preparingDataSet()
{
        bool process_data = false;
        bool process_mc = true;
        bool mva = true;
        bool apply_presel_cuts = true;
        bool apply_additional_info = true;
        bool apply_additional_cuts = true;
        bool apply_additional_variables = true;

        std::string data_base_path = "/eos/lhcb/user/p/petruccs/";
        std::string data_path = data_base_path + "data/bzero_jpsi_etap/data/";
        std::string mc_path = data_base_path + "data/bzero_jpsi_etap/mc/";
        std::string data_title = "_data_bzero_jpsi_etaprime.root";
        std::string mc_title = "_mc_bzero_jpsi_etaprime_no_beauty_match.root";

        std::vector<int> data_years = {2016};
        std::vector<int> mc_years = {2016};

        std::vector<std::string> data_names;
        std::vector<std::string> mc_names;

        TCut presel_time_chi2_cuts = "Bs_DTF_chi2[0] / Bs_DTF_nDOF[0] < 5 && "
                                     "Bs_DTF_ctau[0] / 0.299792458 > 0.3";
        TCut presel_hlt1_cuts = "(Jpsi_Hlt1DiMuonHighMassDecision_TOS || Bs_Hlt1TrackMuonDecision_TOS || Bs_Hlt1TwoTrackMVADecision_TOS)";
        TCut presel_hlt2_cuts = "Jpsi_Hlt2DiMuonDetachedJPsiDecision_TOS";

        TCut presel_cuts = presel_time_chi2_cuts + presel_hlt1_cuts + presel_hlt2_cuts;

        TCut cleaning_cuts = "!TMath::IsNaN(logdira) && TMath::Finite(logdira) && "
                             "!TMath::IsNaN(logvchi2) && TMath::Finite(logvchi2)";
        TCut pid_cut = "abs(rec_jpsi_kk_mass - 5366) < 25 ? "
                       "pi1_PROBNNpi * (1 - pi1_PROBNNk) > 0.75 && pi2_PROBNNpi * (1 - pi2_PROBNNk) > 0.75 :"
                       "pi1_PROBNNpi * (1 - pi1_PROBNNk) > 0.25 && pi2_PROBNNpi * (1 - pi2_PROBNNk) > 0.25";
        TCut jpsi_pipi_cut = "abs(rec_jpsi_pipi_mass - 5279) > 22 && abs(rec_jpsi_pipi_mass - 5366) > 30";
        TCut jpsi_kstar_cut = "abs(rec_jpsi_pipi_mass - 5279) > 24";
        TCut jpsi_kk_cut = "abs(rec_jpsi_kk_mass - 5279) > 20";
        TCut additional_cuts = pid_cut + jpsi_pipi_cut + jpsi_kstar_cut;
        TCut mva_bkg_cut;
        TCut mva_sig_cut;

        // Truth-matching MC
        TCut my_jpsi = "mu1_MC_MOTHER_KEY == mu2_MC_MOTHER_KEY && "
                       "abs(mu1_MC_MOTHER_ID) == 443 && "
                       "abs(mu1_MC_GD_MOTHER_ID) == 531 && "
                       "abs(mu1_TRUEID) == 13 && "
                       "abs(mu2_TRUEID) == 13";

        TCut my_gamma = "gamma_TRUEID == 22 && "
                        "abs(gamma_MC_MOTHER_ID) == 331 && "
                        "abs(gamma_MC_GD_MOTHER_ID) == 531";

        TCut my_etap_pions = "abs(pi1_TRUEID) == 211 && "
                             "abs(pi2_TRUEID) == 211 && "
                             "pi1_MC_MOTHER_KEY == pi2_MC_MOTHER_KEY && "
                             "abs(pi1_MC_MOTHER_ID) == 113 && "
                             "abs(pi1_MC_GD_MOTHER_ID) == 331 && "
                             "abs(pi1_MC_GD_GD_MOTHER_ID) == 531 && "
                             "pi1_MC_GD_MOTHER_KEY == gamma_MC_MOTHER_KEY";

        TCut etap_truthmatch = my_jpsi + my_gamma + my_etap_pions;

        if (mva)
        {
                mva_bkg_cut = "Bs_DTF_M[0] > 6000";
                mva_sig_cut = "abs(Bs_TRUEID) == 531";
        }

        // Generating the names
        for (auto const &year : data_years)
                data_names.push_back(data_path + to_string(year) + data_title);
        for (auto const &year : mc_years)
                mc_names.push_back(mc_path + to_string(year) + mc_title);
        // Applying the first cuts
        if (apply_presel_cuts)
        {
                if (process_data)
                {
                        for (auto const &iname : data_names)
                        {
                                std::string input_file = iname;
                                std::string output_file = iname.substr(0, iname.size() - 5) + "_presel.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                cuttingRootFile(input_file, "DecayTree", output_file, presel_cuts);
                        }
                }
                if (process_mc)
                {
                        for (auto const &iname : mc_names)
                        {
                                std::string input_file = iname;
                                std::string output_file = iname.substr(0, iname.size() - 5) + "_presel.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                cuttingRootFile(input_file, "B2JpsiEtap2Rho0G_Tuple/DecayTree", output_file, presel_cuts + etap_truthmatch);
                        }
                }
        }
        // Applying addInfoEtap
        if (apply_additional_info)
        {
                if (process_data)
                {
                        for (auto const &iname : data_names)
                        {
                                std::string input_file = iname.substr(0, iname.size() - 5) + "_presel.root";
                                std::string output_file = input_file.substr(0, input_file.size() - 5) + "_info.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                addInfoEtap(input_file, "DecayTree", output_file);
                        }
                }
                if (process_mc)
                {
                        for (auto const &iname : mc_names)
                        {
                                std::string input_file = iname.substr(0, iname.size() - 5) + "_presel.root";
                                std::string output_file = input_file.substr(0, input_file.size() - 5) + "_info.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                addInfoEtap(input_file, "DecayTree", output_file);
                        }
                }
        }
        // Generating variables for cuts
        if (apply_additional_variables)
        {
                if (process_data)
                {
                        for (auto const &iname : data_names)
                        {
                                std::string input_file = iname.substr(0, iname.size() - 5) + "_presel_info.root";
                                std::string output_file = input_file.substr(0, input_file.size() - 5) + "_vars.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                add_vars_etap(input_file, "DecayTree", output_file);
                        }
                }
                if (process_mc)
                {
                        for (auto const &iname : mc_names)
                        {
                                std::string input_file = iname.substr(0, iname.size() - 5) + "_presel_info.root";
                                std::string output_file = input_file.substr(0, input_file.size() - 5) + "_vars.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                add_vars_etap(input_file, "DecayTree", output_file);
                        }
                }
        }
        // Applying additional cuts
        if (apply_additional_cuts)
        {
                if (process_data)
                {
                        for (auto const &iname : data_names)
                        {
                                std::string input_file = iname.substr(0, iname.size() - 5) + "_presel_info_vars.root";
                                std::string output_file = input_file.substr(0, input_file.size() - 5) + "_add_cuts.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                cuttingRootFile(input_file, "DecayTree", output_file, additional_cuts);
                                make_info_training(output_file, "DecayTree", "_info_training.root");
                        }
                }
                if (process_mc)
                {
                        for (auto const &iname : mc_names)
                        {
                                std::string input_file = iname.substr(0, iname.size() - 5) + "_presel_info_vars.root";
                                std::string output_file = input_file.substr(0, input_file.size() - 5) + "_add_cuts.root";
                                std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                                cuttingRootFile(input_file, "DecayTree", output_file, additional_cuts);
                                make_info_training(output_file, "DecayTree", "_info_training.root");
                        }
                }
        }
        if (mva)
        {
                for (auto const &iname : data_names)
                {
                        std::string input_file = iname.substr(0, iname.size() - 5) + "_presel_info_vars_add_cuts.root";
                        std::string output_file = input_file.substr(0, input_file.size() - 5) + "_bkg_mva.root";
                        std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                        cuttingRootFile(input_file, "DecayTree", output_file, mva_bkg_cut);
                        make_info_training(output_file, "DecayTree", "_info_training.root");
                }
                for (auto const &iname : mc_names)
                {
                        std::string input_file = iname.substr(0, iname.size() - 5) + "_presel_info_vars_add_cuts.root";
                        std::string output_file = input_file.substr(0, input_file.size() - 5) + "_sig_mva.root";
                        std::cout << "[DEBUG] processing " << input_file << " into " << output_file << std::endl;
                        cuttingRootFile(input_file, "DecayTree", output_file, mva_sig_cut);
                        make_info_training(output_file, "DecayTree", "_info_training.root");
                }
        }
}
