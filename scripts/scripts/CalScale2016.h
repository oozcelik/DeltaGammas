#ifndef CalScale2016_H
#define CalScale2016_H

#include "TH2D.h"
#include "TAxis.h"
#include "TMath.h"
#include "TObject.h"
#include "TRandom.h"
#include <iostream>
#include <iomanip>

class CalScale2016{

 public:

  //  ClassDef(CalScale2015,1);

  CalScale2016(TH2D* phisto, TH2D* mhisto,  double delta = -0.143e-3);

  ~CalScale2016(){} 

  double eval(int run, double tx, double ty, int idp) const ;

  double interpolate(int run, double tx, double ty, int idp) const;

  bool inside(const double tx, const double ty, const TH2D* histo) const;

 private:

  int binTx(const double tx, const TH2D* histo) const;
  int binTy(const double ty, const TH2D* histo) const;
  double runOffset(int run) const;

  
  TH2D* m_phisto;  
  TH2D* m_mhisto;  
  double m_delta;

};


inline double CalScale2016::eval(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  int txBin = binTx(tx,histo);
  int tyBin = binTy(ty,histo);
  //  std::cout << histo->GetBinContent(txBin,tyBin)/1000. << " " << runOffset(run)<< std::endl; 
  return 1- ((histo->GetBinContent(txBin,tyBin)/1000.) + runOffset(run) + m_delta);
}

inline double CalScale2016::interpolate(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  double scale;
  if (inside(tx,ty, histo)) {
    scale =  1- ((histo->Interpolate(tx,ty)/1000.) + runOffset(run) + m_delta);
  }
  else {
    // at the edge fall back to bin method
    scale = eval(run,tx,ty,idp);
  }
  return scale;
}


inline CalScale2016::CalScale2016(TH2D* phisto, TH2D* mhisto, double delta): m_phisto(phisto),m_mhisto(mhisto), m_delta(delta) {
  // constructor  
}

inline bool CalScale2016::inside(const double tx, const double ty, const TH2D* histo) const{
   int ix = histo->GetXaxis()->FindBin(tx);
   if (ix == 0 || ix ==  histo->GetNbinsX()+1 ) return false;
   int iy = histo->GetYaxis()->FindBin(ty);
   if (iy == 0 || iy ==  histo->GetNbinsY()+1 ) return false;
   return true;
}

inline int CalScale2016::binTx(const double tx, const TH2D* histo) const {
  int i = histo->GetXaxis()->FindBin(tx);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsX() ) i = histo->GetNbinsX();
  return i;
}

inline int CalScale2016::binTy(const double ty, const TH2D* histo) const {
  int i = histo->GetYaxis()->FindBin(ty);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsY() ) i = histo->GetNbinsY();
  return i;
}

double CalScale2016::runOffset(int run) const {

  double alpha = 0.0;

 //down
  if (run >=174500   && run <= 175100 ) alpha = 0;
  else if (run >= 175101  && run <= 175900 )  alpha =0.0302171e-3 ;
  else if (run >= 175901  && run <= 176100 )  alpha =0.0558559e-3 ;
  else if (run >= 176101  && run <= 176330 )  alpha = 0.0874364e-3;

  //up
  else if (run >= 176657 && run <= 177000) alpha = 0.130490e-3;
  else if (run >= 177001 && run <= 177250) alpha = 0.146611e-3;
  else if (run >= 177251 && run <= 177350) alpha =  0.166163e-3;
  else if (run >= 177351 && run <= 177500) alpha =  0.153974e-3;
  
  else if (run >= 177501 && run <= 177600) alpha =  0.239742e-3;
  else if (run >= 177601 && run <= 177800) alpha = 0.299172e-3;
  
  else if (run >= 178001 && run <= 178250) alpha = 0.238894e-3;
  else if (run >= 178251 && run <= 178500) alpha = 0.221683e-3;
  else if (run >= 178501 && run <= 179001) alpha = 0.232447e-3;
  else if (run >= 179001 && run <= 179500) alpha = 0.251556e-3;
  else if (run >= 179501 && run <= 179999) alpha = 0.246533e-3;
  else if (run >= 180000 && run <= 180300) alpha = 0.270993e-3;
  else if (run >= 180301 && run <= 180640) alpha = 0.255366e-3;

  // down
  else if (run >= 181200  && run <= 181700 )  alpha = 0.0686362e-3 ;
  else if (run >= 181700  && run <= 182300 )  alpha =0.0945660e-3;
  else if (run >= 182301  && run <= 182500 )  alpha =0.0683000e-3 ;
  else if (run >= 182501  && run <= 182800 )  alpha = 0.0877048e-3;
  else if (run >= 182801  && run <= 183000 )  alpha = 0.126342e-3;
  else if (run >= 183001  && run <= 183200 )  alpha = 0.119248e-3;

  //up
  else if (run >= 183800  && run <= 184200 )  alpha =0.222877e-3 ;
  else if (run >= 184201  && run <= 184657 )  alpha = 0.211130e-3;

  
  // down
  else if (run >= 184700  && run <= 185000 )  alpha =0.2111e-3 ;
  else if (run >= 185000  && run <= 185100 )  alpha =0.05129e-3 ;
  else if (run >= 185101  && run <= 185200 )  alpha = -0.00853040e-3 ;
  else if (run >= 185201  && run <= 185300 )  alpha =0.0154273e-3 ;
  else if (run >= 185301  && run <= 185400 )  alpha =0.0351537e-3 ;
  else if (run >= 185401  && run <= 185600 )  alpha = 0.0677423e-3 ;

  
  return alpha;
  



}


#endif
