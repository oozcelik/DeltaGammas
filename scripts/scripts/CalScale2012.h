#ifndef CalScale2012_H
#define CalScale2012_H

#include "TH2D.h"
#include "TAxis.h"
#include "TMath.h"
//#include "TObject.h"
#include "TRandom.h"
#include <iostream>
#include <iomanip>

class CalScale2012{

 public:

  // ClassDef(CalScale2012,1);

  CalScale2012(TH2D* phisto, TH2D* mhisto,  double delta =  1.8e-4);

  ~CalScale2012(){} 

  double eval(int run, double tx, double ty, int idp) const ;

  double interpolate(int run, double tx, double ty, int idp) const;

  bool inside(const double tx, const double ty, const TH2D* histo) const;

 private:

  int binTx(const double tx, const TH2D* histo) const;
  int binTy(const double ty, const TH2D* histo) const;
  double runOffset(int run) const;

  
  TH2D* m_phisto;  
  TH2D* m_mhisto;  
  double m_delta;

};


inline double CalScale2012::eval(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  int txBin = binTx(tx,histo);
  int tyBin = binTy(ty,histo);
  //  std::cout << histo->GetBinContent(txBin,tyBin)/1000. << " " << runOffset(run)<< std::endl; 
  return 1- ((histo->GetBinContent(txBin,tyBin)/1000.) + runOffset(run) + m_delta);
}

inline double CalScale2012::interpolate(int run, double tx, double ty, int idp) const {
  TH2D* histo = m_phisto;
  if (idp < 0) histo = m_mhisto;
  double scale;
  if (inside(tx,ty, histo)) {
    scale =  1- ((histo->Interpolate(tx,ty)/1000.) + runOffset(run) + m_delta);
  }
  else {
    // at the edge fall back to bin method
    scale = eval(run,tx,ty,idp);
  }
  return scale;
}


inline CalScale2012::CalScale2012(TH2D* phisto, TH2D* mhisto, double delta): m_phisto(phisto),m_mhisto(mhisto), m_delta(delta) {
  // constructor  
}

inline bool CalScale2012::inside(const double tx, const double ty, const TH2D* histo) const{
   int ix = histo->GetXaxis()->FindBin(tx);
   if (ix == 0 || ix ==  histo->GetNbinsX()+1 ) return false;
   int iy = histo->GetYaxis()->FindBin(ty);
   if (iy == 0 || iy ==  histo->GetNbinsY()+1 ) return false;
   return true;
}

inline int CalScale2012::binTx(const double tx, const TH2D* histo) const {
  int i = histo->GetXaxis()->FindBin(tx);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsX() ) i = histo->GetNbinsX();
  return i;
}

inline int CalScale2012::binTy(const double ty, const TH2D* histo) const {
  int i = histo->GetYaxis()->FindBin(ty);
  if (i == 0) i = 1;
  if( i > histo->GetNbinsY() ) i = histo->GetNbinsY();
  return i;
}

inline double CalScale2012::runOffset(int run) const {

  double alpha = 0.0;
  if (run >= 111183   && run <= 114205 ) alpha = 0.0 ; 
  else if (run >= 114205  && run <= 114287 ) alpha =  0.00010737  ; 
  else if (run >= 114316  && run <= 115464  ) alpha =  -2.07091e-05  ; 
  else if (run >= 115518 && run <=117103 ) alpha =     6.97577e-05 ; 
  else if (run >= 117192 && run <= 118286   )alpha =   3.75156e-05    ; 
  else if (run >= 118326   && run <=123803    )alpha =  0.000184355  ; 
  else if (run >=123910   && run <= 125115  ) alpha = 8.61644e-05  ; 
  else if (run >= 125566  && run <= 126680  ) alpha =    0.000241232 ; 
  else if (run >=126824  && run <=  128110 ) alpha =  0.000189161 ; 
  else if (run >=128411   && run <= 129978 ) alpha = 0.000207109;  
  else if (run >= 130316  && run <= 130861) alpha =     3.77e-05  ; 
  else if (run >= 130911    && run <= 131940  ) alpha =   0.000237956  ; 
  else if (run >=  131973 && run <= 133588   ) alpha =    -8.25769e-06 ; 
  else if (run >= 133624  && run <= 134000  )alpha =     0.000220474  ;

  return alpha;  

}



#endif
